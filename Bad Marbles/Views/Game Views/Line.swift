//
//  Line.swift
//  Bad Marbles
//
//  Created by Tyler Green on 5/8/18.
//  Copyright © 2018 Tyler Green. All rights reserved.
//

import UIKit

class Line: UIImageView {
    override init(image: UIImage?) {
        super.init(image: image)
        initLook()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initLook()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initLook()
    }

    func startMoving(){
        UIView.animate(withDuration: Globals.marbleTime, delay: 0.1, options: [.curveLinear],  animations: {
            self.frame.origin = CGPoint(x: self.frame.origin.x, y: Constants.marbleDistance)
        }, completion: { (finished: Bool) in
            self.removeFromSuperview()
        })
    }
    
    func getValue() -> Int{
        return 0
    }
    
    private func initLook(){
        backgroundColor = UIColor.white
    }
}
