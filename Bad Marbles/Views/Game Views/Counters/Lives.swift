//
//  Lives.swift
//  Bad Marbles
//
//  Created by Tyler Green on 4/21/18.
//  Copyright © 2018 Tyler Green. All rights reserved.
//

import UIKit

class Lives: UIView{
    private var totalLives = 3
    private var lives: Int!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initLives()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        initLives()
    }
    
    func addLives(amount: Int){
        Sounds.play(sound: Sounds.lifeGained, soundType: .sound)
        
        if (lives == totalLives){
            totalLives = totalLives + 1
            lives = totalLives
        }
        else{
            lives = lives + 1
        }
        initLives()
    }
    
    func removeLives(amount: Int, isBomb: Bool){
        if (isEmpty()){
            return
        }
        
        Sounds.addVibration()
        
        Sounds.play(sound: isBomb ? Sounds.explosion : Sounds.lifeLost, soundType: .sound)
        if (lives - amount <= 0){
            lives = 0
        }
        else{
            lives = lives - amount
        }
        
        initLives()
    }
    
    func isEmpty() -> Bool{
        return lives <= 0
    }
    
    func resetLives(){
        totalLives = 3
        
        if (MiscUnlockableService.isExtraLifeEnabled()){
            totalLives = totalLives + 1
        }
        
        if (MiscUnlockableService.isExtraLife2Enabled()){
            totalLives = totalLives + 1
        }
        
        lives = totalLives
        
        initLives()
    }
    
    func getRemainingLives() -> Int{
        return lives
    }
    
    private func initLives(){
        frame = CGRect(x: Constants.labelPadding, y: 0, width: (superview?.frame.width ?? UIScreen.main.bounds.width) - Constants.labelPadding * 2, height: superview?.frame.height ?? 115)
        
        subviews.forEach({$0.removeFromSuperview()})
        
        if var livesCounter = lives{
            var xPosition = frame.maxX - Constants.lifeWidth - 10
            
            for _ in 0...(totalLives - 1){
                let heart = UIImageView(image: livesCounter > 0 ? #imageLiteral(resourceName: "heart") : #imageLiteral(resourceName: "empty_heart"))
                
                heart.layer.masksToBounds = false
                heart.layer.shadowRadius = 2.0
                heart.layer.shadowOpacity = 0.2
                heart.layer.shadowOffset = CGSize(width: 1, height: 2)
                heart.layer.minificationFilter = kCAFilterTrilinear
                
                heart.frame = CGRect(x: xPosition, y: frame.midY - Constants.lifeHeight/2 + 15, width: Constants.lifeWidth, height: Constants.lifeHeight)
                
                xPosition = xPosition - 5 - Constants.lifeWidth
                livesCounter = livesCounter - 1
                addSubview(heart)
            }
        }
    }
}
