//
//  Score.swift
//  Bad Marbles
//
//  Created by Tyler Green on 4/21/18.
//  Copyright © 2018 Tyler Green. All rights reserved.
//

import UIKit

class Score: UILabel{
    private var score: Int = 0
    
    override func layoutSubviews() {
        frame = CGRect(x: Constants.labelPadding, y: 15, width: (superview?.frame.width ?? UIScreen.main.bounds.width) - Constants.labelPadding * 2, height: superview?.frame.height ?? 130)
        textColor = UIColor.white
        font = UIFont.systemFont(ofSize: 27.5, weight: .semibold)
        addShadow()
    }
    
    func addScore(amount: Int){
        score = score + amount
        updateLabel()
    }
    
    func resetScore(){
        score = 0
        updateLabel()
    }
    
    func getScore() -> Int{
        return score
    }
    
    func updateLabel(){
        let streak = Globals.streak + 1
        let streakMessage =  " (" + String(describing: streak/5 + 1) + "x)"
        text = score.format() + (streak >= 5 ? streakMessage : "")
    }
}
