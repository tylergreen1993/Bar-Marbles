//
//  LevelCounter.swift
//  Bad Marbles
//
//  Created by Tyler Green on 9/22/18.
//  Copyright © 2018 Tyler Green. All rights reserved.
//

import UIKit

class LevelCounter{
    private var board: Board
    private var level = 1
    
    init(board: Board){
        self.board = board
    }
    
    func getLevel() -> Int{
        return level
    }
    
    func updateNewLevel(){
        level = level + 1
        addLevelLabel()
    }
    
    func resetLevel(){
        level = 1
    }
    
    private func addLevelLabel(){
        let boardFrame = board.getBoard().frame
        let labelHeight: CGFloat = boardFrame.height/7.4
        
        let backgroundView = MovingView()
        backgroundView.frame = CGRect(x: 0, y: -labelHeight, width: boardFrame.width, height: labelHeight)
        backgroundView.alpha = 0.95
        
        board.addToBoard(view: backgroundView)
        
        GradientService.addGradient(view: backgroundView, firstColor: Colors.scoreGreen, secondColor: Colors.darkGreen)
        
        let levelLabel = UILabel()
        levelLabel.text = "Tier " + level.format()
        levelLabel.textColor = UIColor.white
        levelLabel.font = UIFont.systemFont(ofSize: 100, weight: .semibold)
        let levelLabelPadding: CGFloat = 160
        levelLabel.frame = CGRect(x: levelLabelPadding/2, y: 0, width: backgroundView.frame.width - levelLabelPadding, height: backgroundView.frame.height)
        levelLabel.adjustsFontSizeToFitWidth = true
        levelLabel.textAlignment = .center
        levelLabel.baselineAdjustment = .alignCenters
        levelLabel.addShadow()
        backgroundView.addSubview(levelLabel)
        
        backgroundView.startMoving()
    }
}
