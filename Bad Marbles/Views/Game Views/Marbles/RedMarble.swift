//
//  RedMarble.swift
//  Bad Marbles
//
//  Created by Tyler Green on 4/21/18.
//  Copyright © 2018 Tyler Green. All rights reserved.
//

import UIKit

class RedMarble: Marble{
    override func initLook() {
        image = MarbleAppearanceService.getMarble(color: .red)
    }
    
    override func getLivesLost() -> Int {
        return 1
    }
    
    override func getColor() -> UIColor{
        return UIColor.red
    }
}
