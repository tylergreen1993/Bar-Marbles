//
//  OrangeMarble.swift
//  Bad Marbles
//
//  Created by Tyler Green on 7/1/18.
//  Copyright © 2018 Tyler Green. All rights reserved.
//

import UIKit

class OrangeMarble: Marble{
    override func initLook() {
        image = MarbleAppearanceService.getMarble(color: .orange)
    }
    
    override func getScore() -> Int{
        return (1 + (Globals.streak / 5)) * 2
    }
    
    override func getColor() -> UIColor{
        return UIColor.orange
    }
}
