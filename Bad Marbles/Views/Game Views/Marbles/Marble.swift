//
//  Marble.swift
//  Bad Marbles
//
//  Created by Tyler Green on 4/21/18.
//  Copyright © 2018 Tyler Green. All rights reserved.
//

import UIKit

class Marble: UIImageView{
    override init(image: UIImage?) {
        super.init(image: image)
        initLook()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initLook()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initLook()
    }
    
    func startMoving(){
        superview?.layoutIfNeeded()
        self.layoutIfNeeded()
        
        UIView.animate(withDuration: Globals.marbleTime, delay: 0, options: [.curveLinear],  animations: {
            self.frame.origin = CGPoint(x: self.frame.origin.x, y: Constants.marbleDistance)
        }, completion: { (finished: Bool) in
            self.removeFromSuperview()
        })
    }
    
    func getScore() -> Int{
        return 0
    }
    
    func getLivesLost() -> Int{
        return 0
    }
    
    func getLivesGained() -> Int{
        return 0
    }
    
    func getColor() -> UIColor{
        return UIColor.clear
    }
    
    func getPresentationLayer(tries: Int = 0) -> CGRect{
        if let presentationLayer = self.layer.presentation(){
            return presentationLayer.frame
        }
        
        if (tries < 10){
            return getPresentationLayer(tries: tries + 1)
        }
        
        return self.frame
    }

    func destroy(){
        self.layer.shadowColor = UIColor.clear.cgColor
        let animationTime  = 0.1
        
        self.stopAnimating()
        self.animationImages = getMarbleExplosionImages()
        self.animationDuration = animationTime
        self.animationRepeatCount = 1
        self.startAnimating()
        
        UIView.animate(withDuration: animationTime, delay: 0, options: [.curveEaseOut],  animations: {
            self.frame = CGRect(x: self.frame.origin.x - self.frame.width/4, y: self.frame.origin.y - self.frame.height/4, width: self.frame.width * 2, height: self.frame.height * 2)
        }, completion: { (finished: Bool) in
            self.removeFromSuperview()
        })
    }
    
    func isGood() -> Bool{
        return (self is GreenMarble || self is OrangeMarble)
    }
    
    func isBad() -> Bool{
        return (self is RedMarble || self is BlueMarble || self is GrayMarble)
    }
    
    func isBomb() -> Bool{
        return self is GrayMarble
    }
    
    func isHeart() -> Bool{
        return self is HeartMarble
    }
    
    internal func initLook(){
    }
    
    private func getMarbleExplosionImages() -> [UIImage]{
        if (self is GreenMarble){
            return [#imageLiteral(resourceName: "Green_Explosion 1"), #imageLiteral(resourceName: "Green_Explosion 2"), #imageLiteral(resourceName: "Green_Explosion 3")]
        }
        if (self is RedMarble || self.isHeart()){
            return [#imageLiteral(resourceName: "Red_Explosion 1"), #imageLiteral(resourceName: "Red_Explosion 2"), #imageLiteral(resourceName: "Red_Explosion 3")]
        }
        if (self is BlueMarble){
            return [#imageLiteral(resourceName: "Blue_Explosion 1"), #imageLiteral(resourceName: "Blue_Explosion 2"), #imageLiteral(resourceName: "Blue_Explosion 3")]
        }
        if (self is OrangeMarble){
            return [#imageLiteral(resourceName: "Orange_Explosion 1"), #imageLiteral(resourceName: "Orange_Explosion 2"), #imageLiteral(resourceName: "Orange_Explosion 3")]
        }
        if (self.isBomb()){
            return [#imageLiteral(resourceName: "Gray_Explosion 1"), #imageLiteral(resourceName: "Gray_Explosion 2"), #imageLiteral(resourceName: "Gray_Explosion 3")]
        }
        return []
    }
}
