//
//  HeartMarble.swift
//  Bad Marbles
//
//  Created by Tyler Green on 9/2/18.
//  Copyright © 2018 Tyler Green. All rights reserved.
//

import UIKit

class HeartMarble: Marble{
    override func initLook() {
        image = #imageLiteral(resourceName: "Heart")
    }
    
    override func getLivesGained() -> Int {
        return 1
    }
    
    override func getColor() -> UIColor{
        return UIColor.clear
    }
}
