//
//  GreenMarble.swift
//  Bad Marbles
//
//  Created by Tyler Green on 4/21/18.
//  Copyright © 2018 Tyler Green. All rights reserved.
//

import UIKit

class GreenMarble: Marble{
    override func initLook() {
        image = MarbleAppearanceService.getMarble(color: .green)
    }
    
    override func getScore() -> Int{
        return 1 + (Globals.streak / 5)
    }
    
    override func getColor() -> UIColor{
        return UIColor.green
    }
}
