//
//  GrayMarble.swift
//  Bad Marbles
//
//  Created by Tyler Green on 9/2/18.
//  Copyright © 2018 Tyler Green. All rights reserved.
//

import UIKit

class GrayMarble: Marble{
    override func initLook() {
        animationImages = [#imageLiteral(resourceName: "Gray Marble3"), #imageLiteral(resourceName: "Gray Marble2"), #imageLiteral(resourceName: "Gray Marble"), #imageLiteral(resourceName: "Gray Marble2")]
        animationDuration = 0.45
        startAnimating()
    }
    
    override func getLivesLost() -> Int {
        return 10
    }
    
    override func getColor() -> UIColor{
        return UIColor.clear
    }
}
