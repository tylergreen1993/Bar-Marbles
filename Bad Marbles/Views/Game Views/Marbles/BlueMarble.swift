//
//  BlueMarble.swift
//  Bad Marbles
//
//  Created by Tyler Green on 4/21/18.
//  Copyright © 2018 Tyler Green. All rights reserved.
//

import UIKit

class BlueMarble: Marble{
    override func initLook() {
        image = MarbleAppearanceService.getMarble(color: .blue)
    }
    
    override func getLivesLost() -> Int {
        return 2
    }
    
    override func getColor() -> UIColor{
        return UIColor.blue
    }
}
