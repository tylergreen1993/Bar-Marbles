//
//  ScoreView.swift
//  Bad Marbles
//
//  Created by Tyler Green on 4/24/18.
//  Copyright © 2018 Tyler Green. All rights reserved.
//

import UIKit

class ScoreView: UIView{
    var score =  Score()
    var lives = Lives()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initLook()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        addSubview(score)
        addSubview(lives)
        initLook()
    }
    
    private func initLook() {
        backgroundColor = Colors.scoreViewBlue
        clipsToBounds = true
        layer.cornerRadius = 15
        
        layer.masksToBounds = false
        layer.shadowRadius = 3
        layer.shadowOpacity = 0.4
        layer.shadowColor = UIColor.white.cgColor
        layer.shadowOffset = CGSize.zero
    }
}
