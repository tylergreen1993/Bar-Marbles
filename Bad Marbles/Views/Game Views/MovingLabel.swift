//
//  MovingView.swift
//  Bad Marbles
//
//  Created by Tyler Green on 9/23/18.
//  Copyright © 2018 Tyler Green. All rights reserved.
//

import UIKit

class MovingView: UIView {
    func startMoving(){
        UIView.animate(withDuration: Globals.marbleTime, delay: 0.1, options: [.curveLinear],  animations: {
            self.frame.origin = CGPoint(x: self.frame.origin.x, y: Constants.marbleDistance)
        }, completion: { (finished: Bool) in
            self.removeFromSuperview()
        })
    }
}
