//
//  FloatingText.swift
//  Bad Marbles
//
//  Created by Tyler Green on 5/9/18.
//  Copyright © 2018 Tyler Green. All rights reserved.
//

import UIKit

class FloatingText: UILabel{
    private var fontSize: CGFloat = 25
    
    override func layoutSubviews() {
        font = UIFont.systemFont(ofSize: fontSize, weight: .semibold)
        textAlignment = .center
        addShadow()
    }
    
    func startFloating(){
        UIView.animate(withDuration: 2, animations: {
            self.frame.origin = CGPoint(x: self.frame.origin.x, y: self.frame.origin.y - 125)
            self.alpha = 0
            self.fontSize = 32
        }, completion: { (finished: Bool) in
            self.removeFromSuperview()
        })
    }
}
