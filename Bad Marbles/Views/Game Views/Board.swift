//
//  Board.swift
//  Bad Marbles
//
//  Created by Tyler Green on 4/22/18.
//  Copyright © 2018 Tyler Green. All rights reserved.
//

import UIKit

class Board{
    private let view: UIView
    private var board: UIImageView!
    private var hideLines = false
    
    private var lines = [Line]()

    init(view: UIView){
        self.view = view
        initializeBoard()
    }
    
    func clearLines(){
        lines.forEach({$0.removeFromSuperview()})
        lines.removeAll()
    }
    
    func addToBoard(view: UIView){
        view.layer.zPosition = 1000
        board.addSubview(view)
    }
    
    func getBoard() -> UIView{
        return board
    }
    
    func hideLines(hide: Bool){
        hideLines = hide
    }
    
    private func initializeBoard(){
        let boardFrame = CGRect(x: Constants.boardPadding, y: 0, width: view.frame.width - (Constants.boardPadding * 2), height:  view.frame.height)
        
        let boardAlpha: CGFloat = 0.8
        
        board = UIImageView(frame: boardFrame)
        board.backgroundColor = UIColor.black
        board.clipsToBounds = true
        board.alpha = boardAlpha
        board.layer.cornerRadius = 12
        
        board.layer.masksToBounds = false
        board.layer.shadowRadius = 3
        board.layer.shadowColor = UIColor.white.cgColor
        board.layer.shadowOpacity = 0.45
        board.layer.shadowOffset = CGSize(width: 1, height: 2)
        
        view.insertSubview(board, at: 0)
        
        let boardBackground = UIImageView(frame: boardFrame)
        boardBackground.backgroundColor = UIColor.white
        boardBackground.alpha = boardAlpha
        boardBackground.clipsToBounds = true
        boardBackground.layer.cornerRadius = 12
        view.insertSubview(boardBackground, at: 0)
        
        addLine()
    }
    
    private func addLine(){
        if (!hideLines){
            let marbleHeight = view.frame.width/7.5
            let lineFrame = CGRect(x: 0, y: -marbleHeight, width: board.frame.width, height: 2)
            let line = Line(frame: lineFrame)
            line.startMoving()
            lines.append(line)
            board.addSubview(line)
        }
        
        Timer.scheduledTimer(withTimeInterval: Globals.marbleGenerateWaitTime - 0.05, repeats: false){ _ in
            self.addLine()
        }
    }
}
