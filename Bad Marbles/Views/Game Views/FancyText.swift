//
//  FancyText.swift
//  Bad Marbles
//
//  Created by Tyler Green on 5/15/18.
//  Copyright © 2018 Tyler Green. All rights reserved.
//

import UIKit

class FancyText: UILabel {
    private var fontSize: CGFloat = 80
    
    override func layoutSubviews() {
        font = UIFont.systemFont(ofSize: fontSize, weight: .semibold)
        textAlignment = .center
        adjustsFontSizeToFitWidth = true
        
        layer.masksToBounds = false
        layer.shadowRadius = 4
        layer.shadowOpacity = 0.9
        layer.shadowColor = textColor.cgColor
        layer.shadowOffset = CGSize.zero
    }
    
    func startAnimating(duration: TimeInterval = 2){
        UIView.animate(withDuration: duration, animations: {
            self.fontSize = 70
            self.alpha = 0
        }, completion: { (finished: Bool) in
            self.removeFromSuperview()
        })
    }
}
