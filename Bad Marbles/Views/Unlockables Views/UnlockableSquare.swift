//
//  UnlockableSquare.swift
//  Bad Marbles
//
//  Created by Tyler Green on 6/23/18.
//  Copyright © 2018 Tyler Green. All rights reserved.
//

import UIKit
import StoreKit

class UnlockableSquare: UIView {
    var points = 0
    var title = ""
    var image = UIImage()
    var category = UnlockableType.misc
    var unlocked =  false
    var selected =  false
    var new = false
    var isPurchased = false
    var id = 0
    var purchaseId = ""
    var pointsToShow = 0
    var isSecret = false
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initNotifications()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initNotifications()
    }
    
    func initLook(){
        subviews.forEach({$0.removeFromSuperview()})
        
        isUserInteractionEnabled = true
        self.layer.cornerRadius = 8
        self.clipsToBounds = true
        
        layer.masksToBounds = false
        layer.shadowRadius = 1.5
        layer.shadowOpacity = 0.4
        layer.shadowOffset = CGSize(width: 1, height: 2)
        
        if (!unlocked){
            backgroundColor = UIColor.gray.withAlphaComponent(0.75)
        }
        else if (category == UnlockableType.misc){
            backgroundColor = UIColor.green.withAlphaComponent(0.5)
        }
        else if (category == UnlockableType.music){
            backgroundColor = UIColor.cyan.withAlphaComponent(0.5)
        }
        else if (category == UnlockableType.backgrounds){
            backgroundColor = UIColor.red.withAlphaComponent(0.5)
        }
        else if (category == UnlockableType.marbles){
            backgroundColor = UIColor.orange.withAlphaComponent(0.5)
        }
        
        if (selected){
            layer.borderWidth = 3
            layer.borderColor = UIColor.white.cgColor
        }
        else{
            layer.borderWidth = 0
        }
        
        let isPurchasedButtonEnabled = !isPurchased && !unlocked
        
        let gradientBG = UIImageView(image: #imageLiteral(resourceName: "SlightGradient"))
        gradientBG.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height)
        gradientBG.layer.minificationFilter = kCAFilterTrilinear
        gradientBG.alpha = 0.4
        gradientBG.layer.cornerRadius = 8
        gradientBG.clipsToBounds = true
        self.addSubview(gradientBG)
        
        let purchaseButton = UIButton()
        purchaseButton.frame = CGRect(x: 25, y: self.frame.height - 40, width: self.frame.width - 50, height: 30)
        purchaseButton.setTitle(isPurchasedButtonEnabled ? getPrice() + " (Optional)" : "Unlocked", for: .normal)
        purchaseButton.titleLabel?.font = UIFont.systemFont(ofSize: 14, weight: .medium)
        purchaseButton.setTitleColor(UIColor.white, for: .normal)
        purchaseButton.backgroundColor = isPurchasedButtonEnabled ? Colors.darkGreen : UIColor.darkGray
        purchaseButton.clipsToBounds = true
        purchaseButton.layer.cornerRadius = 6
        purchaseButton.isEnabled = isPurchasedButtonEnabled
        purchaseButton.addTarget(self, action: #selector(purchaseProduct(_:)), for: .touchUpInside)
        self.addSubview(purchaseButton)
        
        let textPadding: CGFloat = 10
        
        let scoreLabel = UILabel()
        scoreLabel.frame = CGRect(x: textPadding, y: purchaseButton.frame.minY -  25, width: self.frame.width - textPadding * 2, height: 20)
        scoreLabel.textColor = UIColor.white
        scoreLabel.attributedText =  getUnlockScoreText(isUnlocked: isPurchasedButtonEnabled)
        scoreLabel.textAlignment = .center
        scoreLabel.font = UIFont.systemFont(ofSize: 14, weight: .bold)
        scoreLabel.adjustsFontSizeToFitWidth = true
        scoreLabel.addShadow()
        self.addSubview(scoreLabel)
        
        let titleLabel = UILabel()
        titleLabel.frame = CGRect(x: textPadding, y: scoreLabel.frame.minY - 25, width: self.frame.width - textPadding * 2, height: 20)
        titleLabel.textColor = UIColor.white
        titleLabel.text = title
        titleLabel.textAlignment = .center
        titleLabel.font = UIFont.systemFont(ofSize: 18, weight: .medium)
        titleLabel.adjustsFontSizeToFitWidth = true
        titleLabel.addShadow()
        self.addSubview(titleLabel)
        
        let imgDimension: CGFloat = 80
        
        let img = UIImageView(image: image)
        img.alpha = unlocked ? 1 : 0.2
        img.frame = CGRect(x: (self.frame.width - imgDimension)/2, y: titleLabel.frame.minY - imgDimension - 20, width: imgDimension, height: imgDimension)
        img.layer.minificationFilter = kCAFilterTrilinear
        img.clipsToBounds = true
        img.layer.cornerRadius = 6
        if (category == .backgrounds){
            img.contentMode = .scaleAspectFill
        }
        self.addSubview(img)
        
        if (new){
            let bannerWidth: CGFloat = 75
            let bannerHeight: CGFloat = 65
            let img = UIImageView(image: #imageLiteral(resourceName: "New Banner"))
            img.frame = CGRect(x: self.frame.width - bannerWidth, y: 0, width: bannerWidth, height: bannerHeight)
            img.layer.minificationFilter = kCAFilterTrilinear
            self.addSubview(img)
        }
        
        if (isSecret){
            purchaseButton.setTitle("???", for: .normal)
            purchaseButton.isEnabled = false
            img.alpha = 0.05
        }
    }
    
    private func initNotifications(){
        NotificationCenter.default.addObserver(self, selector: #selector(self.productPurchasedSuccessfully(_:)), name: NSNotification.Name(rawValue: "PurchaseSuccessful"), object: nil)
    }
    
    private func getPrice() -> String{
        for product in IAPService.shared.getProducts(){
            if (product.productIdentifier == purchaseId){
                if let formattedPrice = product.formattedPrice(){
                    return formattedPrice
                }
            }
        }
        
        return "Buy"
    }
    
    private func getUnlockScoreText(isUnlocked: Bool) -> NSAttributedString{
        let scoreAttribute = [NSAttributedStringKey.foregroundColor: Colors.scoreGreen]
        let unlockedText =  NSMutableAttributedString(string: isUnlocked ? "Unlocked at " : points.format() + " points")
        let scoreText = NSMutableAttributedString(string: isUnlocked ? (isSecret ? "???" : points.format()) + " points" : "", attributes: scoreAttribute)
        
        unlockedText.append(scoreText)
        
        return unlockedText
    }

    @objc private func purchaseProduct(_ sender: UIButton){
        IAPService.shared.purchaseProduct(productId: self.purchaseId)
    }
    
    @objc private func productPurchasedSuccessfully(_ notification: NSNotification) {
        if let productID = notification.userInfo?["productID"] as? String{
            if (productID == purchaseId){
                isPurchased = true
                unlocked = true
                initLook()
                
            }
        }
    }
}
