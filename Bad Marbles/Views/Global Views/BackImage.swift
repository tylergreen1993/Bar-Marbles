//
//  BackImage.swift
//  Bad Marbles
//
//  Created by Tyler Green on 6/2/18.
//  Copyright © 2018 Tyler Green. All rights reserved.
//

import UIKit

class BackImage: UIImageView {
    override init(image: UIImage?) {
        super.init(image: image)
        initBackFunctionality()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initBackFunctionality()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initBackFunctionality()
    }
    
    func initBackFunctionality(){
        isUserInteractionEnabled = true
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(backToMenu(_:)))
        addGestureRecognizer(tap)
    }
    
    @objc private func backToMenu(_ sender: UITapGestureRecognizer){
        Sounds.play(sound: Sounds.woosh, soundType: .sound)
        
        if let window = superview?.window{
            window.rootViewController?.dismiss(animated: true, completion: nil)
        }
    }
}
