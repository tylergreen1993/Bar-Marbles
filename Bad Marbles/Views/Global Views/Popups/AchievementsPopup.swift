//
//  AchievementsPopup.swift
//  Bad Marbles
//
//  Created by Tyler Green on 10/27/18.
//  Copyright © 2018 Tyler Green. All rights reserved.
//

import UIKit

class AchievementsPopup: Popup {
    func showPopup(){
        let popupWidth: CGFloat = 300
        let popupHeight: CGFloat = 300
        initPopup(popupWidth: popupWidth, popupHeight: popupHeight)

        let scrollPadding: CGFloat = 12.5
        let scrollHeight: CGFloat = 240
        
        let scrollView = UIScrollView()
        scrollView.frame =  CGRect(x: scrollPadding, y: 0, width: popup.frame.width - scrollPadding * 2, height: scrollHeight)
        scrollView.contentInset = UIEdgeInsetsMake(scrollPadding, 0, 0, 0)
        scrollView.showsVerticalScrollIndicator = false
        popup.addSubview(scrollView)
        
        populateScrollBar(scrollView: scrollView)
        
        let buttonWidth: CGFloat = 120
        let buttonHeight: CGFloat = 40
        
        let hideButton = UIButton()
        hideButton.frame = CGRect(x: 15, y: popup.frame.height - buttonHeight - 15, width: buttonWidth, height: buttonHeight)
        hideButton.setTitle("Hide", for: .normal)
        hideButton.titleLabel?.font = UIFont.systemFont(ofSize: 18, weight: .semibold)
        hideButton.backgroundColor = Colors.logoRed
        hideButton.clipsToBounds = true
        hideButton.layer.cornerRadius = 8
        hideButton.addTarget(self, action: #selector(hidePopup(_:)), for: .touchUpInside)
        popup.addSubview(hideButton)
        
        let statisticsButton = UIButton()
        statisticsButton.frame = CGRect(x: hideButton.frame.maxX + 30, y: popup.frame.height - buttonHeight - 15, width: buttonWidth, height: buttonHeight)
        statisticsButton.setTitle("Statistics", for: .normal)
        statisticsButton.titleLabel?.font = UIFont.systemFont(ofSize: 18, weight: .semibold)
        statisticsButton.backgroundColor = Colors.softPurple
        statisticsButton.clipsToBounds = true
        statisticsButton.layer.cornerRadius = 8
        statisticsButton.addTarget(self, action: #selector(showStatistics(_:)), for: .touchUpInside)
        popup.addSubview(statisticsButton)
        
        UIView.animate(withDuration: 0.25, animations: {
            self.blackScreen.alpha = 0.75
        }, completion: { (finished: Bool) in
            self.fullView.addSubview(self.popup)
        })
    }
    
    private func populateScrollBar(scrollView: UIScrollView){
        var y: CGFloat = 0
        
        var achievements = AchievementsService.getAchievements()
        achievements.sort(by: {(($1.ratio >= 1).description,  $0.category.rawValue, $0.difficulty.rawValue) < (($0.ratio >= 1).description, $1.category.rawValue, $1.difficulty.rawValue)})
        
        let unlockedAchievements = achievements.filter({$0.ratio >= 1})
        let percentUnlocked = Int(Double(unlockedAchievements.count)/Double(achievements.count) * 100)
        
        let percentUnlockedLabel = UILabel()
        percentUnlockedLabel.frame = CGRect(x: 5, y: 0, width: scrollView.frame.width - 20, height: 50)
        percentUnlockedLabel.text = percentUnlocked.format() + "% Total Achieved"
        percentUnlockedLabel.font = UIFont.systemFont(ofSize: 22, weight: .semibold)
        percentUnlockedLabel.textColor = percentUnlocked == 100 ? Colors.scoreGreen : UIColor.white
        percentUnlockedLabel.addShadow()
        scrollView.addSubview(percentUnlockedLabel)
        
        y = y + percentUnlockedLabel.frame.height
        
        let trophies: [AchievementDifficulty] = [.bronze, .silver, .gold]
        let trophyHeight: CGFloat = 28.5
        
        for trophy in trophies{
            var x: CGFloat = 5
            var trophyImage = #imageLiteral(resourceName: "Bronze Trophy")
            var trophyAchievements = achievements.filter({$0.difficulty == .bronze || $0.difficulty == .bronze2})
            
            if (trophy == .silver){
                x = 100
                trophyImage = #imageLiteral(resourceName: "Silver Trophy")
                trophyAchievements = achievements.filter({$0.difficulty == .silver || $0.difficulty == .silver2})
            }
            else if (trophy == .gold){
                x = 195
                trophyImage = #imageLiteral(resourceName: "Gold Trophy")
                trophyAchievements = achievements.filter({$0.difficulty == .gold})
            }
            
            let unlockedTrophyAchievements = trophyAchievements.filter({$0.ratio >= 1})
            let percentTrophyUnlocked = Int(Double(unlockedTrophyAchievements.count)/Double(trophyAchievements.count) * 100)
            
            let trophyImageView = UIImageView(image: trophyImage)
            trophyImageView.frame = CGRect(x: x, y: y, width: trophyHeight * 0.875, height: trophyHeight)
            trophyImageView.layer.minificationFilter = kCAFilterTrilinear
            scrollView.addSubview(trophyImageView)
            
            let trophyPercentLabel = UILabel()
            trophyPercentLabel.text = "- " + percentTrophyUnlocked.format() + "%"
            trophyPercentLabel.frame = CGRect(x: trophyImageView.frame.maxX + 5, y: y, width: 70, height: trophyHeight)
            trophyPercentLabel.textColor = UIColor.white
            trophyPercentLabel.font = UIFont.systemFont(ofSize: 15, weight: .semibold)
            trophyPercentLabel.addShadow()
            scrollView.addSubview(trophyPercentLabel)
        }
        
        y = y + trophyHeight + 12.5
        
        for achievement in achievements{
            var trophyImage = #imageLiteral(resourceName: "Bronze Trophy")
            var trophyType = "Bronze"
            var trophyColor = UIColor.lightText
            
            if (achievement.difficulty == .silver || achievement.difficulty == .silver2){
                trophyImage = #imageLiteral(resourceName: "Silver Trophy")
                trophyType = "Silver"
                trophyColor = UIColor.lightGray
            }
            else if (achievement.difficulty == .gold){
                trophyImage = #imageLiteral(resourceName: "Gold Trophy")
                trophyType = "Gold"
                trophyColor = UIColor.yellow
            }
            
            let achievementBackdrop = UIImageView()
            achievementBackdrop.frame = CGRect(x: 0, y: y, width: scrollView.frame.width, height: 65)
            achievementBackdrop.backgroundColor = UIColor.black.withAlphaComponent(0.2)
            achievementBackdrop.layer.cornerRadius = 8
            achievementBackdrop.clipsToBounds = true
            scrollView.addSubview(achievementBackdrop)
            
            let trophyIcon = UIImageView(image: trophyImage)
            trophyIcon.frame = CGRect(x: 10, y: 12.5, width: 35, height: 40)
            trophyIcon.layer.minificationFilter = kCAFilterTrilinear
            achievementBackdrop.addSubview(trophyIcon)
            
            let typeLabel = UILabel()
            typeLabel.frame = CGRect(x: 10, y: 0, width: scrollView.frame.width - 20, height: 20)
            typeLabel.text = trophyType
            typeLabel.textAlignment = .right
            typeLabel.textColor = trophyColor
            typeLabel.font = UIFont.systemFont(ofSize: 14, weight: .semibold)
            typeLabel.addShadow()
            achievementBackdrop.addSubview(typeLabel)
            
            let achievementLabel = UILabel()
            achievementLabel.frame = CGRect(x: trophyIcon.frame.width + 20, y: 12.5, width: scrollView.frame.width - trophyIcon.frame.width - 20, height: trophyIcon.frame.height)
            achievementLabel.text = achievement.name
            achievementLabel.font = UIFont.systemFont(ofSize: 20, weight: .semibold)
            achievementLabel.textAlignment = .center
            achievementLabel.textColor = UIColor.white
            achievementLabel.addShadow()
            achievementBackdrop.addSubview(achievementLabel)
            
            if (achievement.ratio >= 1){
                achievementBackdrop.layer.borderWidth = 1
                achievementBackdrop.layer.borderColor = UIColor.lightText.cgColor
            }
            else{
                achievementBackdrop.alpha = 0.5
                
                let progressBar = UIProgressView()
                progressBar.frame = CGRect(x: achievementLabel.frame.minX, y: achievementLabel.frame.maxY + 2.5, width: achievementLabel.frame.width - 10, height: 3)
                progressBar.setProgress(achievement.ratio, animated: true)
                progressBar.tintColor = UIColor.white
                progressBar.progressTintColor = Colors.scoreGreen
                achievementBackdrop.addSubview(progressBar)
            }
            
            y = y + achievementBackdrop.frame.height + 5
        }
        
        scrollView.contentSize = CGSize(width: scrollView.frame.width, height: y + 15)
    }
    
    @objc private func hidePopup(_ sender: UIButton){
        hide()
    }
    
    @objc private func showStatistics(_ sender: UIButton){
        StatisticsService.showInfoPopup()
    }
}
