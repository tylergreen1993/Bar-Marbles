//
//  TutorialPopup.swift
//  Bad Marbles
//
//  Created by Tyler Green on 5/24/18.
//  Copyright © 2018 Tyler Green. All rights reserved.
//

import UIKit

class TutorialPopup: Popup{
    private var currentScreen = CurrentScreen.first
    private let tutorialKey = "TutorialSeen"
    private var demo = false
    
    init(fullView: UIView, demo: Bool = false){
        self.demo = demo
        super.init(fullView: fullView)
    }
    
    func showPopup() -> Bool{
        let tutorialSeen = UserDefaults.standard.bool(forKey: tutorialKey)
        if (!tutorialSeen){
            forcePopup()
            UserDefaults.standard.set(true, forKey: tutorialKey)
            return true
        }
        
        return false
    }
    
    func forcePopup(){
        let popupWidth: CGFloat = 300
        let popupHeight: CGFloat = 300
        initPopup(popupWidth: popupWidth, popupHeight: popupHeight, sound: demo)
        
        let tutorialLabel = UILabel()
        tutorialLabel.frame = CGRect(x: 10, y: 25, width: popupWidth - 20, height: 60)
        tutorialLabel.text = (currentScreen == .first ? "Tap on bad, passing marbles to destroy them." : "Destroy ONLY bad marbles. Good marbles that pass score points.")
        tutorialLabel.textColor = UIColor.white
        tutorialLabel.textAlignment = .center
        tutorialLabel.font = UIFont.systemFont(ofSize: 16, weight: .semibold)
        tutorialLabel.numberOfLines = 10
        tutorialLabel.adjustsFontSizeToFitWidth = true
        popup.addSubview(tutorialLabel)
        
        let tutorialImageWidth: CGFloat = currentScreen == .first ? 150 : 200
        let tutorialImageHeight: CGFloat = currentScreen == .first ? 75 : 99
        
        let tutorialImage = UIImageView()
        tutorialImage.frame = CGRect(x: (popup.frame.width - tutorialImageWidth)/2, y: tutorialLabel.frame.maxY + 20, width: tutorialImageWidth, height: tutorialImageHeight)
        tutorialImage.animationImages = (currentScreen == .first ? [#imageLiteral(resourceName: "Tutorial 1"), #imageLiteral(resourceName: "Tutorial 2")] : [#imageLiteral(resourceName: "Tutorial 3")])
        tutorialImage.animationDuration = 1
        tutorialImage.startAnimating()
        popup.addSubview(tutorialImage)
        
        let buttonWidth: CGFloat = 120
        let buttonHeight: CGFloat = 40
        
        let hideButton = UIButton()
        hideButton.frame = CGRect(x: (popup.frame.width - buttonWidth)/2, y: popup.frame.height - buttonHeight - 15, width: buttonWidth, height: buttonHeight)
        hideButton.setTitle((currentScreen == .first ? "Next" : "Got it!"), for: .normal)
        hideButton.titleLabel?.font = UIFont.systemFont(ofSize: 18, weight: .semibold)
        hideButton.setTitleColor(UIColor.white, for: .normal)
        hideButton.backgroundColor = Colors.softPurple
        hideButton.clipsToBounds = true
        hideButton.layer.cornerRadius = 8
        hideButton.addTarget(self, action: #selector(hidePopup(_:)), for: .touchUpInside)
        popup.addSubview(hideButton)
        
        UIView.animate(withDuration: currentScreen == .first ? 0.25 : 0, animations: {
            self.blackScreen.alpha = 0.75
        }, completion: { (finished: Bool) in
            self.fullView.addSubview(self.popup)
        })
    }
    
    @objc private func hidePopup(_ sender: UIButton){
        hide(sound: currentScreen == .second)
        
        if (currentScreen == .first){
            currentScreen = .second
            forcePopup()
            return
        }
        
        currentScreen = .first

        if (!demo){
            NotificationCenter.default.post(name: Notification.Name("RestartGame"), object: nil)
        }
    }
}

enum CurrentScreen{
    case first
    case second
}
