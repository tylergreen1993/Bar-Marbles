//
//  GameOverPopup.swift
//  Bad Marbles
//
//  Created by Tyler Green on 5/19/18.
//  Copyright © 2018 Tyler Green. All rights reserved.
//

import UIKit
import NotificationBannerSwift
import GoogleMobileAds

class GameOverPopup: Popup{
    private let unlockablesRepository = UnlockablesRepository()
    private let viewController: UIViewController
    private var gameEngine: GameEngine!
    private var extraChanceButton: UIButton!
    private var banner: NotificationBanner!
    private var lastScore = 0

    init(fullView: UIView, viewController: UIViewController){
        self.viewController = viewController
        super.init(fullView: fullView)
    }
    
    func showPopup(score: Int, rewardAdSeen: RewardAdSeen, is2xExtraChanceEnabled: Bool){
        let popupWidth: CGFloat = 300
        let popupHeight: CGFloat = 300
        initPopup(popupWidth: popupWidth, popupHeight: popupHeight)
        
        var showHighScoreImage = false
        var highScore = ScoreService.getHighScore()
        if (score > highScore){
            highScore = score
            ScoreService.setHighScore(score: highScore)
            
            showHighScoreImage = true
        }
        
        let pointsToAdd = rewardAdSeen != .none ? score - lastScore : score
        ScoreService.setTotalPoints(score: pointsToAdd)
        
        lastScore = score

        let highScoreLabel = UILabel()
        highScoreLabel.frame = CGRect(x: 0, y: 30, width: popupWidth, height: 120)
        highScoreLabel.textColor = UIColor.white
        highScoreLabel.attributedText = getHighScoreText(highScore: highScore)
        highScoreLabel.textAlignment = .center
        highScoreLabel.numberOfLines = 2
        highScoreLabel.font = UIFont.systemFont(ofSize: 45, weight: .semibold)
        highScoreLabel.adjustsFontSizeToFitWidth = true
        highScoreLabel.addShadow()
        popup.addSubview(highScoreLabel)
        
        if (showHighScoreImage){
            let highScoreBannerWidth: CGFloat = 250
            let highScoreBannerHeight: CGFloat = 86
            
            let highScoreBanner = ShareImage(image: #imageLiteral(resourceName: "HighScoreBanner"))
            highScoreBanner.frame = CGRect(x: (blackScreen.frame.width - highScoreBannerWidth)/2, y: popup.frame.minY - highScoreBannerHeight - 10, width: highScoreBannerWidth, height: highScoreBannerHeight)
            highScoreBanner.layer.minificationFilter = kCAFilterTrilinear
            highScoreBanner.setViewController(viewController: viewController)
            blackScreen.addSubview(highScoreBanner)
            
            let newHighScoreWidth: CGFloat = 45
            let newHighScoreHeight: CGFloat = 27
            
            let newHighScoreImage = UIImageView(image: #imageLiteral(resourceName: "New Score"))
            newHighScoreImage.frame = CGRect(x: popup.frame.width - newHighScoreWidth - 12, y: 20, width: newHighScoreWidth, height: newHighScoreHeight)
            newHighScoreImage.layer.minificationFilter = kCAFilterTrilinear
            popup.addSubview(newHighScoreImage)
        }
        
        let shareIconWidth: CGFloat = 38
        let shareIconHeight: CGFloat = 27
        
        let shareIcon = ShareImage(image: #imageLiteral(resourceName: "Share"))
        shareIcon.setViewController(viewController: viewController)
        shareIcon.frame = CGRect(x: 10, y: 7.5, width: shareIconWidth, height: shareIconHeight)
        popup.addSubview(shareIcon)
        
        let pointsLabel = UILabel()
        pointsLabel.frame = CGRect(x: 0, y: highScoreLabel.frame.maxY + 7.5, width: popupWidth, height: 70)
        pointsLabel.text = "Points Earned\n " + score.format()
        pointsLabel.textColor = UIColor.white
        pointsLabel.textAlignment = .center
        pointsLabel.numberOfLines = 2
        pointsLabel.font = UIFont.systemFont(ofSize: 30, weight: .semibold)
        pointsLabel.adjustsFontSizeToFitWidth = true
        pointsLabel.addShadow()
        popup.addSubview(pointsLabel)

        let buttonWidth: CGFloat = 120
        let buttonHeight: CGFloat = 40
        
        let restartButton = UIButton()
        restartButton.frame = CGRect(x: 15, y: popup.frame.height - buttonHeight - 15, width: buttonWidth, height: buttonHeight)
        restartButton.setTitle("Play Again", for: .normal)
        restartButton.titleLabel?.font = UIFont.systemFont(ofSize: 18, weight: .semibold)
        restartButton.backgroundColor = Colors.logoRed
        restartButton.clipsToBounds = true
        restartButton.layer.cornerRadius = 8
        restartButton.addTarget(self, action: #selector(restartGame(_:)), for: .touchUpInside)
        popup.addSubview(restartButton)
        
        let menuButton = BadgeButton()
        menuButton.frame = CGRect(x: restartButton.frame.maxX + 30, y: popup.frame.height - buttonHeight - 15, width: buttonWidth, height: buttonHeight)
        menuButton.setTitle("Main Menu", for: .normal)
        menuButton.titleLabel?.font = UIFont.systemFont(ofSize: 18, weight: .semibold)
        menuButton.backgroundColor = Colors.softPurple
        menuButton.clipsToBounds = true
        menuButton.layer.cornerRadius = 8
        menuButton.addTarget(self, action: #selector(backToMenu(_:)), for: .touchUpInside)
        popup.addSubview(menuButton)
        
        let prevUnlockablesCount = unlockablesRepository.getUnlockablesByMaxPoints(points: ScoreService.getLastCheckedTotalPoints()).count
        let prevUnlockblesCountOnUnlockablePage = unlockablesRepository.getUnlockablesByMaxPoints(points: ScoreService.getLastCheckedTotalPointsUnlockables()).count
        let newUnlockablesCount = unlockablesRepository.getUnlockablesByMaxPoints(points: ScoreService.getTotalPoints()).count

        if (newUnlockablesCount > prevUnlockblesCountOnUnlockablePage){
            menuButton.badge = "+" + (newUnlockablesCount - prevUnlockblesCountOnUnlockablePage).format()
        }
        
        if (newUnlockablesCount > prevUnlockablesCount){
            let amountOfUnlockablesUnlocked = newUnlockablesCount - prevUnlockablesCount
            banner = NotificationBanner(title: "New Unlockable" + (amountOfUnlockablesUnlocked == 1 ? "" : "s") + "!", subtitle: "Check your unlockables to see " + (amountOfUnlockablesUnlocked == 1 ? "it!" : "which!"), leftView: UIImageView(image: #imageLiteral(resourceName: "Marbles Trophy")), style: .success)
            banner.duration = 30
            banner.onTap = {
                self.backToMenu(menuButton)
            }
            banner.show()
        }
        
        extraChanceButton = UIButton()
        extraChanceButton.frame = CGRect(x: popup.frame.minX, y: popup.frame.maxY + 15, width: popupWidth, height: buttonHeight)
        extraChanceButton.setTitle("Watch an Ad for an Extra Chance", for: .normal)
        extraChanceButton.titleLabel?.font = UIFont.systemFont(ofSize: 18, weight: .semibold)
        extraChanceButton.backgroundColor = rewardAdSeen == .none ? Colors.darkGreen : Colors.badMarblesBlue
        extraChanceButton.clipsToBounds = true
        extraChanceButton.layer.cornerRadius = 8
        extraChanceButton.addTarget(self, action: #selector(extraChanceButtonClicked(_:)), for: .touchUpInside)
        extraChanceButton.isHidden = is2xExtraChanceEnabled ? (rewardAdSeen == .twice) : (rewardAdSeen == .once)
        fullView.addSubview(extraChanceButton)
        
        UIView.animate(withDuration: 0.25, animations: {
            self.blackScreen.alpha = 0.75
        }, completion: { (finished: Bool) in
            self.fullView.addSubview(self.popup)
        })
    }

    func setGameEngine(gameEngine: GameEngine){
        self.gameEngine = gameEngine
    }
    
    func hide(){
        super.hide()
        extraChanceButton.removeFromSuperview()
    }
    
    private func getHighScoreText(highScore: Int) -> NSAttributedString{
        let scoreAttribute = [NSAttributedStringKey.foregroundColor: Colors.scoreGreen]
        let highScoreText =  NSMutableAttributedString(string: "High Score\n")
        let scoreText = NSMutableAttributedString(string: highScore.format(), attributes: scoreAttribute)
    
        highScoreText.append(scoreText)
        
        return highScoreText
    }

    @objc private func restartGame(_ sender: UIButton){
        hide()
        banner?.dismiss()
        NotificationCenter.default.post(name: Notification.Name("RestartGame"), object: nil)
    }
    
    @objc private func backToMenu(_ sender: UIButton){
        Sounds.play(sound: Sounds.woosh, soundType: .sound)
        Sounds.stopAllMusic()
        gameEngine == nil ? print("No game engine set on game over popup.") : NotificationCenter.default.removeObserver(gameEngine)
        banner?.dismiss()
        viewController.dismiss(animated: true, completion: nil)
    }
    
    @objc private func extraChanceButtonClicked(_ sender: UIButton){
        if (GADRewardBasedVideoAd.sharedInstance().isReady){
            Sounds.play(sound: Sounds.woosh, soundType: .sound)
            Sounds.stopAllMusic()
            
            let viewWindow = UIWindow(frame: UIScreen.main.bounds)
            viewWindow.rootViewController = UIViewController()
            viewWindow.windowLevel = UIWindowLevelAlert + 1
            viewWindow.makeKeyAndVisible()
            
            if let vc = viewWindow.rootViewController{
                GADRewardBasedVideoAd.sharedInstance().present(fromRootViewController: vc)
            }
        }
        else{
            AlertService.showAlert(title: "Ad Hasn't Loaded", message: "Wait a few seconds and try again. Otherwise check your internet connection.")
            DispatchQueue.main.asyncAfter(deadline: .now() + 3){
                if (!GADRewardBasedVideoAd.sharedInstance().isReady){
                        AdService.loadRewardAd()
                }
            }
        }
    }
}
