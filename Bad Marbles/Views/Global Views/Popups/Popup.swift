//
//  Popup.swift
//  Bad Marbles
//
//  Created by Tyler Green on 10/25/18.
//  Copyright © 2018 Tyler Green. All rights reserved.
//

import UIKit

class Popup{
    internal let fullView: UIView
    internal var popup: UIView!
    internal var blackScreen: UIView!
    internal var showing =  false
    
    init(fullView: UIView){
        self.fullView = fullView
    }
    
    internal func initPopup(popupWidth: CGFloat, popupHeight: CGFloat, sound: Bool = true){
        if (sound){
            Sounds.play(sound: Sounds.woosh, soundType: .sound)
        }
        showing = true
        
        popup = UIView()
        blackScreen = UIView(frame: fullView.frame)
        
        blackScreen.backgroundColor = UIColor.black
        blackScreen.alpha = 0
        fullView.addSubview(blackScreen)
        
        popup.frame = CGRect(x: (fullView.frame.width - popupWidth)/2, y: (fullView.frame.height - popupHeight)/2, width: popupWidth, height: popupHeight)
        popup.backgroundColor = Colors.scoreViewBlue
        popup.clipsToBounds = true
        popup.layer.cornerRadius = 8
        popup.layer.borderWidth = 1
        popup.layer.borderColor = UIColor.white.cgColor
    }
    
    internal func isShowing() -> Bool{
        return showing
    }
    
    internal func hide(sound: Bool = true){
        blackScreen.removeFromSuperview()
        popup.removeFromSuperview()
        showing = false
        
        if (sound){
            Sounds.play(sound: Sounds.woosh, soundType: .sound)
        }
    }
}
