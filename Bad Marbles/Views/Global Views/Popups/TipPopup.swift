//
//  TipPopup.swift
//  Bad Marbles
//
//  Created by Tyler Green on 10/11/18.
//  Copyright © 2018 Tyler Green. All rights reserved.
//

import UIKit

class TipPopup : Popup{
    let tipItems = TipService.shared.getTipItems()
    
    func showPopup(){
        let popupWidth: CGFloat = 300
        let popupHeight: CGFloat = 300
        initPopup(popupWidth: popupWidth, popupHeight: popupHeight)
        
        let tipLabel = UILabel()
        tipLabel.text = "If you like the game, a small tip is always appreciated!"
        tipLabel.frame = CGRect(x: 15, y: 10, width: popupWidth - 30, height: 60)
        tipLabel.textColor = UIColor.white
        tipLabel.font = UIFont.systemFont(ofSize: 18, weight: .semibold)
        tipLabel.numberOfLines = 2
        tipLabel.textAlignment = .center
        popup.addSubview(tipLabel)
        
        let scrollPadding: CGFloat = 15
        let scrollHeight: CGFloat = 160
        
        let sideScroll = UIScrollView()
        sideScroll.showsHorizontalScrollIndicator = false
        sideScroll.frame =  CGRect(x: 0, y: tipLabel.frame.maxY + 5, width: popup.frame.width, height: scrollHeight)
        sideScroll.contentInset = UIEdgeInsetsMake(0, scrollPadding, 0, 0)
        sideScroll.backgroundColor = UIColor.black.withAlphaComponent(0.15)
        popup.addSubview(sideScroll)
        
        populateSideScroll(sideScroll: sideScroll)
        
        let buttonWidth: CGFloat = 120
        let buttonHeight: CGFloat = 40
        
        let hideButton = UIButton()
        hideButton.frame = CGRect(x: (popup.frame.width - buttonWidth)/2, y: popup.frame.height - buttonHeight - 15, width: buttonWidth, height: buttonHeight)
        hideButton.setTitle("Hide", for: .normal)
        hideButton.titleLabel?.font = UIFont.systemFont(ofSize: 18, weight: .semibold)
        hideButton.setTitleColor(UIColor.white, for: .normal)
        hideButton.backgroundColor = Colors.softPurple
        hideButton.clipsToBounds = true
        hideButton.layer.cornerRadius = 8
        hideButton.addTarget(self, action: #selector(hidePopup(_:)), for: .touchUpInside)
        popup.addSubview(hideButton)
        
        UIView.animate(withDuration: 0.25, animations: {
            self.blackScreen.alpha = 0.75
        }, completion: { (finished: Bool) in
            self.fullView.addSubview(self.popup)
        })
    }
    
    private func populateSideScroll(sideScroll: UIScrollView){
        let width: CGFloat = 100
        var x: CGFloat = 0
        var index  = 0
        for tipItem in tipItems{
            let tipImage = UIImageView(image: tipItem.image)
            tipImage.frame = CGRect(x: x + 15, y: 7.5, width: width - 30, height: width - 30)
            tipImage.layer.minificationFilter = kCAFilterTrilinear
            sideScroll.addSubview(tipImage)
            
            let tipName = UILabel()
            tipName.text = tipItem.name
            tipName.frame = CGRect(x: x, y: tipImage.frame.maxY + 10, width: width, height: 25)
            tipName.textColor = UIColor.white
            tipName.font = UIFont.systemFont(ofSize: 18, weight: .semibold)
            tipName.textAlignment = .center
            tipName.adjustsFontSizeToFitWidth = true
            tipName.addShadow()
            sideScroll.addSubview(tipName)
            
            let tipPriceButton = UIButton()
            tipPriceButton.setTitle(tipItem.price, for: .normal)
            tipPriceButton.frame = CGRect(x: x, y: tipName.frame.maxY + 10, width: width, height: 30)
            tipPriceButton.backgroundColor = Colors.darkGreen
            tipPriceButton.setTitleColor(UIColor.white, for: .normal)
            tipPriceButton.titleLabel?.font = tipName.font
            tipPriceButton.clipsToBounds = true
            tipPriceButton.layer.cornerRadius = 8
            tipPriceButton.tag = index
            tipPriceButton.addTarget(self, action: #selector(purchaseProduct(_:)), for: .touchUpInside)
            sideScroll.addSubview(tipPriceButton)

            index = index + 1
            x =  x + width + 15
        }
        
        sideScroll.contentSize = CGSize(width: x + 15, height: sideScroll.frame.height)
    }
    
    @objc private func purchaseProduct(_ sender: UIButton){
        if (sender.tag < tipItems.count){
            TipService.shared.purchaseProduct(productId: tipItems[sender.tag].purchaseId)
        }
    }
    
    @objc private func hidePopup(_ sender: UIButton){
        hide()
    }
}
