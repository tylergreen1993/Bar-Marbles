//
//  UnlockablesTutorialPopup.swift
//  Bad Marbles
//
//  Created by Tyler Green on 10/25/18.
//  Copyright © 2018 Tyler Green. All rights reserved.
//

import UIKit

class UnlockablesTutorialPopup: Popup {
    private let tutorialKey = "UnlockablesTutorialSeen"
    
    func showPopup(){
        let tutorialSeen = UserDefaults.standard.bool(forKey: tutorialKey)
        if (!tutorialSeen){
            forcePopup()
            UserDefaults.standard.set(true, forKey: tutorialKey)
            return
        }
    }
    
    func forcePopup(){
        let popupWidth: CGFloat = 300
        let popupHeight: CGFloat = 300
        initPopup(popupWidth: popupWidth, popupHeight: popupHeight, sound: false)
        
        let tutorialLabel = UILabel()
        tutorialLabel.frame = CGRect(x: 10, y: 25, width: popupWidth - 20, height: 80)
        tutorialLabel.text = "Earn awesome unlockables by racking up points in-game (or optionally buying them)!"
        tutorialLabel.textColor = UIColor.white
        tutorialLabel.textAlignment = .center
        tutorialLabel.font = UIFont.systemFont(ofSize: 16, weight: .semibold)
        tutorialLabel.numberOfLines = 10
        tutorialLabel.adjustsFontSizeToFitWidth = true
        popup.addSubview(tutorialLabel)
        
        let tutorialImageWidth: CGFloat = 90
        let tutorialImageHeight: CGFloat = 90
        
        let extraLifeImage = #imageLiteral(resourceName: "ExtraLife")
        let tutorialImage = UIImageView(image: extraLifeImage)
        tutorialImage.frame = CGRect(x: (popup.frame.width - tutorialImageWidth)/2, y: tutorialLabel.frame.maxY + 20, width: tutorialImageWidth, height: tutorialImageHeight)
        tutorialImage.layer.minificationFilter = kCAFilterTrilinear
        popup.addSubview(tutorialImage)
        
        let buttonWidth: CGFloat = 120
        let buttonHeight: CGFloat = 40
        
        let hideButton = UIButton()
        hideButton.frame = CGRect(x: (popup.frame.width - buttonWidth)/2, y: popup.frame.height - buttonHeight - 15, width: buttonWidth, height: buttonHeight)
        hideButton.setTitle("Got it!", for: .normal)
        hideButton.titleLabel?.font = UIFont.systemFont(ofSize: 18, weight: .semibold)
        hideButton.setTitleColor(UIColor.white, for: .normal)
        hideButton.backgroundColor = Colors.softPurple
        hideButton.clipsToBounds = true
        hideButton.layer.cornerRadius = 8
        hideButton.addTarget(self, action: #selector(hidePopup(_:)), for: .touchUpInside)
        popup.addSubview(hideButton)
        
        UIView.animate(withDuration: 0.25, animations: {
            self.blackScreen.alpha = 0.75
        }, completion: { (finished: Bool) in
            self.fullView.addSubview(self.popup)
        })
    }
    
    @objc private func hidePopup(_ sender: UIButton){
        hide()
    }
}
