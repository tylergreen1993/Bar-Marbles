//
//  ShareImage.swift
//  Bad Marbles
//
//  Created by Tyler Green on 7/18/18.
//  Copyright © 2018 Tyler Green. All rights reserved.
//

import UIKit

class ShareImage: UIImageView {
    private var viewController: UIViewController!
    
    override init(image: UIImage?) {
        super.init(image: image)
        initProperties()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initProperties()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initProperties()
    }
    
    func setViewController(viewController: UIViewController){
        self.viewController = viewController
    }
    
    private func initProperties(){
        layer.minificationFilter = kCAFilterTrilinear
        isUserInteractionEnabled = true
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(showSharePrompt(_:)))
        addGestureRecognizer(tap)
    }
    
    @objc private func showSharePrompt(_ sender: UITapGestureRecognizer){
        if (viewController == nil){
            print("No view controller set on share icon.")
            return
        }
        
        if let view =  sender.view as? ShareImage{
            Sounds.play(sound: Sounds.woosh, soundType: .sound)
            
            let highScore = ScoreService.getHighScore()
            let textToShare = "Hey! I got a high score of " + highScore.format() + " in Bad Marbles. Download the game and try to beat it: " + Resources.iTunesLink + "."
            
            UIGraphicsBeginImageContextWithOptions(viewController.view.frame.size, true, 0.0)
            viewController.view.drawHierarchy(in: viewController.view.frame, afterScreenUpdates: false)
            let img = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            
            if let img = img{
                let shareObjects = [img, textToShare] as [Any]
                
                let activityVC = UIActivityViewController(activityItems: shareObjects, applicationActivities: nil)
                activityVC.excludedActivityTypes = [.saveToCameraRoll]
                
                activityVC.popoverPresentationController?.sourceView = view
                viewController.present(activityVC, animated: true, completion: nil)
            }
        }
    }
}
