//
//  Cloud.swift
//  Bad Marbles
//
//  Created by Tyler Green on 7/7/18.
//  Copyright © 2018 Tyler Green. All rights reserved.
//

import UIKit

class Cloud: UIImageView {
    override init(image: UIImage?) {
        super.init(image: image)
        initProperties()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initProperties()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initProperties()
    }
    
    func startMoving(){
        if let view = superview{
            UIView.animate(withDuration: 6.5, delay: 0, options: [.curveLinear], animations: {
                self.frame.origin = CGPoint(x: view.frame.minX - self.frame.width - 5 , y: self.frame.origin.y)
            }, completion: { (finished: Bool) in
                let selectedYPoint = CGFloat(arc4random_uniform(UInt32(self.frame.height))) + self.frame.height/2
                self.frame.origin = CGPoint(x: view.frame.maxX + self.frame.width, y: selectedYPoint)
                self.startMoving()
            })
        }
    }
    
    private func initProperties(){
        layer.minificationFilter = kCAFilterTrilinear
    }
}
