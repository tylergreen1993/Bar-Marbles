//
//  AchievementIcon.swift
//  Bad Marbles
//
//  Created by Tyler Green on 10/26/18.
//  Copyright © 2018 Tyler Green. All rights reserved.
//

import UIKit

class AchievementIcon: UIImageView {
    var achievementsPopup: AchievementsPopup!
    var fullView = UIView()
    
    override init(image: UIImage?) {
        super.init(image: image)
        initProperties()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initProperties()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initProperties()
    }
    
    func updateImage(){
        let achievements = AchievementsService.getAchievements()
        let achievementsCount = achievements.count
        let unlockedAchievementsCount = achievements.filter({$0.ratio >= 1}).count
        let lastSeenUnlockedAchievementsCount = AchievementsService.getLastSeenUnlockedCount()
        
        var newImage = #imageLiteral(resourceName: "White Trophy")
        
        if (achievementsCount == unlockedAchievementsCount){
            newImage = #imageLiteral(resourceName: "Gold Trophy")
        }
        else if (lastSeenUnlockedAchievementsCount < unlockedAchievementsCount){
            newImage = #imageLiteral(resourceName: "Red Trophy")
        }
        
        self.image = newImage
    }
    
    func initPopup(fullView: UIView){
        self.fullView = fullView
        achievementsPopup = AchievementsPopup(fullView: fullView)
    }

    private func initProperties(){
        layer.minificationFilter = kCAFilterTrilinear
        isUserInteractionEnabled = true
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(achievementPressed(_:)))
        addGestureRecognizer(tap)
    }
    
    @objc private func achievementPressed(_ sender: UITapGestureRecognizer){
        if (achievementsPopup != nil){
            achievementsPopup.showPopup()
            AchievementsService.updateLastSeenUnlockedCount()
            updateImage()
        }
    }
}
