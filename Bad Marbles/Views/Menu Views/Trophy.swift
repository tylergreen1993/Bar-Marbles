//
//  Trophy.swift
//  Bad Marbles
//
//  Created by Tyler Green on 7/19/18.
//  Copyright © 2018 Tyler Green. All rights reserved.
//

import UIKit

class Trophy: UIImageView {
    private var totalPoints = 0
    let pointsForGoldTrophy = 35000
    
    override init(image: UIImage?) {
        super.init(image: image)
        initProperties()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initProperties()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initProperties()
    }
    
    func checkIfEnabled(){
        totalPoints = ScoreService.getTotalPoints()
        image = totalPoints < pointsForGoldTrophy ? #imageLiteral(resourceName: "Marbles Trophy Silver") : #imageLiteral(resourceName: "Marbles Trophy")
        isHidden = !MiscUnlockableService.isMarblesTrophyEnabled()
    }

    private func initProperties(){
        layer.minificationFilter = kCAFilterTrilinear
        isUserInteractionEnabled = true
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(trophyPressed(_:)))
        addGestureRecognizer(tap)
    }
    
    @objc private func trophyPressed(_ sender: UITapGestureRecognizer){
        if let marblesTrophy = Sounds.marblesTrophy{
            if (!marblesTrophy.playing){
                Sounds.stopAllMusic()
                Sounds.play(sound: marblesTrophy, soundType: .music)
            }
        }
        
        let pointsRemaining = pointsForGoldTrophy - totalPoints
        
        var message = pointsRemaining > 0 ? "\nHmm... I bet someone with " + pointsForGoldTrophy.format() + " points (" + pointsRemaining.format() + " away) would have an even nicer trophy." : "\nCongrats! You are the champion of Bad Marbles! Now go play outside or read a book."
        if (totalPoints >= 100000){
            message = totalPoints.format() + " points! Now that's impressive!"
        }
        AlertService.showAlert(title: "Marbles Trophy", message: message)
        
        if (pointsRemaining <= 0){
            Sounds.play(sound: Sounds.goldTrophy, soundType: .sound)
        }
    }
}
