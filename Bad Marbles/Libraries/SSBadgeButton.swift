//
//  BadgeButton.swift
//  Bad Marbles
//
//  Created by Tyler Green on 7/5/18.
//  Copyright © 2018 Tyler Green. All rights reserved.
//

import UIKit

class BadgeButton: UIButton {
    var badgeLabel = UILabel()
    
    var badge: String? {
        didSet {
            addBadgeToButton(badge: badge)
        }
    }
    
    public var badgeBackgroundColor = UIColor.red {
        didSet {
            badgeLabel.backgroundColor = badgeBackgroundColor
        }
    }
    
    public var badgeTextColor = UIColor.white {
        didSet {
            badgeLabel.textColor = badgeTextColor
        }
    }
    
    public var badgeFont = UIFont.systemFont(ofSize: 13.5, weight: .semibold) {
        didSet {
            badgeLabel.font = badgeFont
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        addBadgeToButton(badge: nil)
    }
    
    func addBadgeToButton(badge: String?) {
        badgeLabel.text = badge
        badgeLabel.textColor = badgeTextColor
        badgeLabel.backgroundColor = badgeBackgroundColor
        badgeLabel.font = badgeFont
        badgeLabel.sizeToFit()
        badgeLabel.textAlignment = .center
        let badgeSize = badgeLabel.frame.size
        
        let height = max(25, badgeSize.height + 5.0)
        let width = max(height, badgeSize.width + 12.0)
        
        badgeLabel.frame = CGRect(x: frame.maxX - width/2 - 10, y: frame.minY - height/2, width: width, height: height)
        
        badgeLabel.layer.cornerRadius = badgeLabel.frame.height/2
        badgeLabel.layer.masksToBounds = true
        superview?.addSubview(badgeLabel)
        badgeLabel.isHidden = badge != nil ? false : true
    }
    
    func removeBadge(){
        badgeLabel.removeFromSuperview()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.addBadgeToButton(badge: nil)
    }
}
