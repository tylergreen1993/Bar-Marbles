//
//  SKProductExtension.swift
//  Bad Marbles
//
//  Created by Tyler Green on 10/11/18.
//  Copyright © 2018 Tyler Green. All rights reserved.
//

import UIKit
import StoreKit

extension SKProduct{
   func formattedPrice() -> String? {
        let numberFormatter = NumberFormatter()
        let locale = priceLocale
        numberFormatter.numberStyle = .currency
        numberFormatter.locale = locale
        return numberFormatter.string(from: price)
    }
}
