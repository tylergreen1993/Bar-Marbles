//
//  ArrayExtensions.swift
//  Bad Marbles
//
//  Created by Tyler Green on 7/5/18.
//  Copyright © 2018 Tyler Green. All rights reserved.
//

import UIKit

extension Array where Element: Hashable {
    func difference(from other: [Element]) -> [Element] {
        let thisSet = Set(self)
        let otherSet = Set(other)
        return Array(thisSet.symmetricDifference(otherSet))
    }
}
