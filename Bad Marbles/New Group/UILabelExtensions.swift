//
//  UILabelExtensions.swift
//  Bad Marbles
//
//  Created by Tyler Green on 9/25/18.
//  Copyright © 2018 Tyler Green. All rights reserved.
//

import UIKit

extension UILabel {
    func addShadow(){
        self.layer.masksToBounds = false
        self.layer.shadowRadius = 2
        self.layer.shadowOpacity = 0.2
        self.layer.shadowOffset = CGSize(width: 1, height: 2)
    }
}
