//
//  UIButtonExtensions.swift
//  Bad Marbles
//
//  Created by Tyler Green on 10/2/18.
//  Copyright © 2018 Tyler Green. All rights reserved.
//

import UIKit

extension UIButton{
    func fitTextToButton(){
        titleLabel?.font = titleLabel?.font.withSize(min(frame.width/7.5, 65))
        titleLabel?.adjustsFontSizeToFitWidth = true
        titleLabel?.baselineAdjustment = .alignCenters
    }
}
