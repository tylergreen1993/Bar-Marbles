//
//  StringExtensions.swift
//  Bad Marbles
//
//  Created by Tyler Green on 7/7/18.
//  Copyright © 2018 Tyler Green. All rights reserved.
//

import UIKit

extension Int{
    func format() -> String{
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = NumberFormatter.Style.decimal
        if let formattedNumber = numberFormatter.string(from: NSNumber(value: self)){
            return formattedNumber
        }
        
        return String(describing: self)
    }
}
