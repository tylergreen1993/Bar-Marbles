//
//  UnlockablesEngine.swift
//  Bad Marbles
//
//  Created by Tyler Green on 6/5/18.
//  Copyright © 2018 Tyler Green. All rights reserved.
//

import UIKit

class UnlockablesEngine{
    private let scrollView: UIScrollView
    private let totalPoints: Int
    private let unlockablesRepository: UnlockablesRepository
    private let categories = [UnlockableCategory(name: "Perks", type: UnlockableType.misc),
                              UnlockableCategory(name: "Backgrounds", type: UnlockableType.backgrounds),
                              UnlockableCategory(name: "Music", type: UnlockableType.music),
                              UnlockableCategory(name: "Marbles", type: UnlockableType.marbles)]
    private var tipPopup: TipPopup!
    private var unlockablesTutorialPopup: UnlockablesTutorialPopup!
    private var currentY: CGFloat = 0
    private var unlockables = [UnlockableSquare]()
    private var newUnlockables = [UnlockableSquare]()
    
    init(fullView: UIView, scrollView: UIScrollView, totalPoints: Int){
        self.scrollView = scrollView
        self.totalPoints = totalPoints
        self.unlockablesRepository = UnlockablesRepository()
        self.tipPopup = TipPopup(fullView: fullView)
        self.unlockablesTutorialPopup = UnlockablesTutorialPopup(fullView: fullView)
    }
    
    func populateUnlockables(){
        unlockables = unlockablesRepository.getUnlockables()
        newUnlockables = unlockablesRepository.getUnlockablesByMaxPoints(points: totalPoints).difference(from: unlockablesRepository.getUnlockablesByMaxPoints(points: ScoreService.getLastCheckedTotalPointsUnlockables()))
        createCategories()
        
        ScoreService.setLastCheckedTotalPointsUnlockables()
        
        scrollView.contentSize = CGSize(width: scrollView.frame.width, height: currentY + 120)
        unlockablesTutorialPopup.showPopup()
    }
    
    private func createCategories(){
        let nextUnlockable = unlockablesRepository.getNextAvailableUnlockable(points: totalPoints)
        addUnlockableText(newUnlockables: newUnlockables, nextUnlockable: nextUnlockable)
        for category in categories{
            var unlockablesByCategory = unlockables.filter({$0.category == category.type})
            unlockablesByCategory.sort(by: {($1.points, $0.title) > ($0.points, $1.title)})
            
            let unlockedUnlockables = unlockablesByCategory.filter({totalPoints >= $0.points || IAPService.shared.isProductPurchased(productId: $0.purchaseId)})
            let newUnlockableImage = addTitleText(text: category.name, smallText: String(describing: unlockedUnlockables.count) + "/" + String(describing: unlockablesByCategory.count), completed: unlockedUnlockables.count == unlockablesByCategory.count)
            
            let sideScroll = addSideScroll()
            
            var x: CGFloat = 0
            
            for unlockable in unlockablesByCategory{
                unlockable.frame = CGRect(x: x, y: 0, width: sideScroll.frame.height, height: sideScroll.frame.height)
                unlockable.isPurchased = IAPService.shared.isProductPurchased(productId: unlockable.purchaseId)
                unlockable.unlocked = totalPoints >= unlockable.points || unlockable.isPurchased
                unlockable.isSecret = unlockable.pointsToShow > totalPoints
                
                if (unlockable.unlocked){
                    unlockable.selected = getSelectedUnlockable(id: unlockable.id, category: unlockable.category)
                }
                
                if (newUnlockables.filter({$0.id == unlockable.id}).count == 1){
                    unlockable.new = true
                    newUnlockableImage.isHidden = false
                }
                
                unlockable.initLook()
                
                let tap = UITapGestureRecognizer(target: self, action: #selector(unlockableTapped(_:)))
                unlockable.addGestureRecognizer(tap)
                
                sideScroll.addSubview(unlockable)
                
                x = x + unlockable.frame.width + 10
            }
            
            sideScroll.contentSize = CGSize(width: x, height: sideScroll.frame.height)
        }
    }
    
    private func addUnlockableText(newUnlockables: [UnlockableSquare], nextUnlockable: UnlockableSquare?){
        let newUnlockablesAmount = newUnlockables.count
        var amountText = NSMutableAttributedString(string: "Awesome! You got " + (newUnlockablesAmount == 1 ? "a new unlockable" : (newUnlockablesAmount.format() + " new unlockables")) + "!")
        
        var tip = false
        if (newUnlockablesAmount == 0){
            if let unlockable = nextUnlockable{
                let unlockableName = unlockable.title
                let pointsAway = unlockable.points - totalPoints
                
                let pointsAttribute = [NSAttributedStringKey.foregroundColor: Colors.scoreGreen]
                let starterText = NSMutableAttributedString(string: pointsAway >= 500 ? "Don't stop! " : "You're close! ")
                amountText = NSMutableAttributedString(string: pointsAway.format() + " point" + (pointsAway == 1 ? "" : "s"), attributes: pointsAttribute)
                let remainingText =  NSMutableAttributedString(string: " left to unlock the \"" + unlockableName + "\" " + getUnlockableCategoryToString(category: unlockable.category).lowercased() + "!")
                amountText.insert(starterText, at: 0)
                amountText.append(remainingText)
            }
            else{
                tip = true
            }
        }
        
        currentY = currentY + 25
        
        let textPadding: CGFloat = 15
        
        if (tip){
            let tipButtonWidth: CGFloat = 70
            let tipButtonHeight: CGFloat = 30
            
            let tipLabel = UILabel()
            tipLabel.textColor = UIColor.white
            tipLabel.font = UIFont.systemFont(ofSize: 18, weight: .semibold)
            tipLabel.text = "If you like the game, consider leaving the creator a small tip to show your support!"
            tipLabel.numberOfLines = 5
            let tipLabelSize = tipLabel.sizeThatFits(CGSize(width: scrollView.frame.width - tipButtonWidth - (textPadding * 3), height: tipLabel.frame.height))
            tipLabel.frame = CGRect(x: textPadding, y: currentY, width: tipLabelSize.width, height: tipLabelSize.height)
            tipLabel.addShadow()
            
            scrollView.addSubview(tipLabel)
            
            let tipButton = UIButton()
            tipButton.setTitle("Tip", for: .normal)
            tipButton.setTitleColor(UIColor.white, for: .normal)
            tipButton.frame = CGRect(x: tipLabel.frame.maxX + textPadding, y: tipLabel.frame.midY - tipButtonHeight/2, width: tipButtonWidth, height: tipButtonHeight)
            tipButton.backgroundColor = Colors.darkGreen
            tipButton.clipsToBounds = true
            tipButton.layer.cornerRadius = 6
            tipButton.addTarget(self, action: #selector(showTipPopup(_:)), for: .touchUpInside)
            
            scrollView.addSubview(tipButton)

            currentY = currentY + tipLabelSize.height
        }
        else{
            let textPadding: CGFloat = 15
            let imageDimension: CGFloat = 60
            
            let exclamationImage = UIImageView(image: newUnlockablesAmount == 0 ? #imageLiteral(resourceName: "Exclamation Mark") : #imageLiteral(resourceName: "Exclamation Mark 2"))
            exclamationImage.frame = CGRect(x: textPadding, y: currentY, width: imageDimension, height: imageDimension)
            exclamationImage.layer.minificationFilter = kCAFilterTrilinear
            scrollView.addSubview(exclamationImage)
            
            let textWidth = scrollView.frame.width - exclamationImage.frame.maxX - textPadding - 10
            let textLabel = UILabel()
            textLabel.textColor = UIColor.white
            textLabel.font = UIFont.systemFont(ofSize: 18, weight: .semibold)
            textLabel.attributedText = amountText
            textLabel.numberOfLines = 5
            let size = textLabel.sizeThatFits(CGSize(width: textWidth, height: textLabel.frame.height))
            textLabel.frame = CGRect(x: textPadding + imageDimension + 10, y: currentY + (imageDimension - size.height)/2, width: size.width, height: size.height)
            textLabel.addShadow()
            
            scrollView.addSubview(textLabel)
            
            currentY = currentY + imageDimension
        }
    }

    private func addTitleText(text: String, smallText: String, completed: Bool) -> UIImageView{
        let textPadding: CGFloat = 15
        let textHeight: CGFloat = 100
        
        let smallTextAttribute = [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 18.5, weight: .semibold), NSAttributedStringKey.foregroundColor: completed ? Colors.scoreGreen : UIColor.white]
        let categoryText =  NSMutableAttributedString(string: text + " ")
        let amountText = NSMutableAttributedString(string: smallText, attributes: smallTextAttribute)
        
        categoryText.append(amountText)

        let textLabel = UILabel()
        textLabel.textColor = UIColor.white
        textLabel.font = UIFont.systemFont(ofSize: 30, weight: .semibold)
        textLabel.attributedText = categoryText
        textLabel.sizeToFit()
        textLabel.frame = CGRect(x: textPadding, y: currentY, width: textLabel.frame.width, height: textHeight)
        textLabel.addShadow()
        
        scrollView.addSubview(textLabel)
        
        let newUnlockableImageWidth: CGFloat = 50
        let newUnlockableImageHeight: CGFloat = 30
        let newUnlockableImage = UIImageView(image: #imageLiteral(resourceName: "New Score"))
        newUnlockableImage.frame = CGRect(x: textLabel.frame.maxX + textPadding, y: currentY + (textHeight - newUnlockableImageHeight)/2, width: newUnlockableImageWidth, height: newUnlockableImageHeight)
        newUnlockableImage.layer.minificationFilter = kCAFilterTrilinear
        newUnlockableImage.isHidden = true
        scrollView.addSubview(newUnlockableImage)
        
        currentY = currentY + textHeight
        
        return newUnlockableImage
    }
    
    private func addSideScroll() -> UIScrollView{
        let height: CGFloat = 200
        let padding: CGFloat = 15
        
        let sideScroll = UIScrollView()
        sideScroll.showsHorizontalScrollIndicator = false
        sideScroll.frame =  CGRect(x: 0, y: currentY, width: scrollView.frame.width, height: height)
        sideScroll.contentInset = UIEdgeInsetsMake(0, padding, 0, 0)
        scrollView.addSubview(sideScroll)
        currentY = currentY + height
        return sideScroll
    }
    
    private func getSelectedUnlockable(id: Int, category: UnlockableType) -> Bool{
        switch category{
        case .music:
            return BackgroundMusicService.getSelectedId() == id
        case .marbles:
            return MarbleAppearanceService.getSelectedId() == id
        case .backgrounds:
            return BackgroundAppearanceService.getSelectedId() == id
        case .misc:
            return MiscUnlockableService.checkForId(id: id)
        }
    }
    
    private func deselectAllInCategory(category: UnlockableType){
        if (category == .misc){
            return
        }
        
        let unlockablesByCategory = unlockables.filter({$0.category == category})
        
        for unlockable in unlockablesByCategory{
            unlockable.selected = false
            unlockable.initLook()
        }
    }
    
    private func getUnlockableCategoryToString(category: UnlockableType) -> String{
        switch category{
        case .misc:
            return "Perk"
        case .music:
            return "Music"
        case .backgrounds:
            return "Background"
        case .marbles:
            return "Marble"
        }
    }

    @objc private func unlockableTapped(_ sender: UITapGestureRecognizer){
        if let unlockable = sender.view as? UnlockableSquare{
            if (unlockable.unlocked && !unlockable.selected){
                Sounds.play(sound: Sounds.select, soundType: .sound)
                Sounds.addVibration()
            }
            else if (unlockable.unlocked && unlockable.category == .misc){
                Sounds.play(sound: Sounds.select, soundType: .sound)
                Sounds.addVibration()
                
                MiscUnlockableService.removeUnlockable(id: unlockable.id)
                unlockable.selected = false
                unlockable.initLook()
                return
            }
            else{
                Sounds.play(sound: Sounds.error, soundType: .sound)
                return
            }
            
            switch unlockable.category{
            case .music:
                BackgroundMusicService.setBackgroundMusic(id: unlockable.id)
            case .marbles:
                MarbleAppearanceService.setMarbleAppearance(id: unlockable.id)
            case .backgrounds:
                BackgroundAppearanceService.setBackground(id: unlockable.id)
            case .misc:
                MiscUnlockableService.setUnlockable(id: unlockable.id)
            }
            
            deselectAllInCategory(category: unlockable.category)
            unlockable.selected = true
            unlockable.initLook()
        }
    }
    
    @objc private func showTipPopup(_ sender: UIButton){
        tipPopup.showPopup()
    }
}

enum UnlockableType{
    case misc, music, backgrounds, marbles
}

private struct UnlockableCategory{
    var name: String
    var type: UnlockableType
    
    init(name: String, type: UnlockableType){
        self.name = name
        self.type = type
    }
}
