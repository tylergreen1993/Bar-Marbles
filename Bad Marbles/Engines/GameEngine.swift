//
//  GameEngine.swift
//  Bad Marbles
//
//  Created by Tyler Green on 4/21/18.
//  Copyright © 2018 Tyler Green. All rights reserved.
//

import UIKit
import GoogleMobileAds
import FacebookCore

class GameEngine: NSObject, GADInterstitialDelegate, GADRewardBasedVideoAdDelegate{
    private let fullView: UIView
    private let gameView: UIView
    private let scoreView: ScoreView
    private let gameController: UIViewController
    private let score: Score
    private let lives: Lives
    private let levelCounter: LevelCounter!
    private let board: Board!
    private let gameOverPopup: GameOverPopup!
    private let tutorialPopup: TutorialPopup!
    private let orangeMarbleTutorialPopup: OrangeMarbleTutorialPopup!
    private var interstitial: GADInterstitial!
    private var tierTimer: Timer!

    private var marbles = [Marble]()
    private var marblesPerScreen = Constants.marblesPerScreen
    private var lastStreakAmount = 0
    private var countDownSecs = Constants.waitToStart
    private var badMarblesHit = 0
    private var isGameReset = false
    private var rewardAdSeen = RewardAdSeen.none
    private var wasRewardWatched = false
    private var isNoBombsEnabled = MiscUnlockableService.isNoBombsEnabled()
    private var isOrangeMarbleEnabled = MiscUnlockableService.isOrangeMarbleEnabled()
    private var isGlowingMarblesEnabled = MiscUnlockableService.isGlowingMarblesEnabled()
    private var isPointedTiersEnabled = MiscUnlockableService.isPointedTiersEnabled()
    private var is2xExtraChanceEnabled = MiscUnlockableService.is2xExtraChanceEnabled()

    init(fullView: UIView, gameView: UIView, scoreView: ScoreView, gameController: UIViewController){
        self.fullView = fullView
        self.gameView = gameView
        self.scoreView = scoreView
        self.gameController = gameController
        self.score = scoreView.score
        self.lives = scoreView.lives
        self.gameOverPopup = GameOverPopup(fullView: fullView, viewController: gameController)
        self.tutorialPopup = TutorialPopup(fullView: fullView)
        self.orangeMarbleTutorialPopup = OrangeMarbleTutorialPopup(fullView: fullView)
        self.board = Board(view: gameView)
        self.levelCounter = LevelCounter(board: board)
        super.init()
        
        self.interstitial = AdService.createAndLoadInterstitial(controller: self)
    }

    func startGame(){
        Sounds.play(sound: Sounds.woosh, soundType: .sound)
        initNotifications()
        Globals.resetGlobals()
        
        let viewPressed = UILongPressGestureRecognizer(target: self, action: #selector(viewPressed(_:)))
        viewPressed.minimumPressDuration = 0
        gameView.addGestureRecognizer(viewPressed)
        
        gameOverPopup.setGameEngine(gameEngine: self)
        let isShowingTutorial = tutorialPopup.showPopup()
        let isShowingOrangeMarbleTutorial = orangeMarbleTutorialPopup.showPopup()
        board.hideLines(hide: isShowingTutorial || isShowingOrangeMarbleTutorial)
        
        AdService.initRewardAds(controller: self)
        
        Timer.scheduledTimer(withTimeInterval: Globals.marbleGenerateWaitTime + 0.2, repeats: false){ _ in
            self.resetGame()
        }
    }
    
    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
        Sounds.play(sound: BackgroundMusicService.getSelectedBackgroundMusic(), soundType: .music)
        interstitial = AdService.createAndLoadInterstitial(controller: self)
    }
    
    func rewardBasedVideoAdDidClose(_ rewardBasedVideoAd: GADRewardBasedVideoAd) {
        Sounds.play(sound: BackgroundMusicService.getSelectedBackgroundMusic(), soundType: .music)
        if (wasRewardWatched){
            continueGameFromReward()
            wasRewardWatched =  false
        }
        AdService.loadRewardAd()
    }
    
    func rewardBasedVideoAd(_ rewardBasedVideoAd: GADRewardBasedVideoAd,
                            didRewardUserWith reward: GADAdReward) {
        wasRewardWatched = true
        rewardAdSeen = getNewRewardAdSeen()
        isGameReset = true
        Globals.marbleTime = min(Globals.marbleTime / pow(Constants.marbleTimeDecrease, 1000), Globals.defaultMarbleTime())
        gameOverPopup.hide()
        destroyAllMarbles()
    }
    
    @objc private func viewPressed(_ longPress: UILongPressGestureRecognizer){
        let pressLocation = longPress.location(in: longPress.view)
        
        if (longPress.state == .began){
            var i = 0
            for marble in marbles{
                let marbleHitBoxPadding: CGFloat =  30
                let marbleFrame = marble.getPresentationLayer()
                let marbleHitBox = CGRect(x: marbleFrame.origin.x - marbleHitBoxPadding/2, y: marbleFrame.origin.y - marbleHitBoxPadding/2, width: marbleFrame.width + marbleHitBoxPadding, height: marbleFrame.height + marbleHitBoxPadding)
                
                if (marbleHitBox.contains(pressLocation)){
                    marble.destroy()
                    marbles.remove(at: i)
                    Sounds.play(sound: Sounds.destroySound, soundType: .sound)
                    if (marble.isBad()){
                        badMarblesHit = badMarblesHit + 1
                    }
                    else if (marble.isHeart()){
                        lives.addLives(amount: marble.getLivesGained())
                        showFloatText(text: "+♥", color: UIColor.green, frame: marbleFrame)
                    }
                    break
                }
                i = i + 1
            }
        }
    }
    
    @objc private func resetGame(){
        Globals.resetGlobals()
        lives.resetLives()
        score.resetScore()
        levelCounter.resetLevel()
        
        if (isTutorialShowing()){
            return
        }
        
        countDownSecs = Constants.waitToStart
        lastStreakAmount = 0
        marblesPerScreen = Constants.marblesPerScreen
        badMarblesHit = 0
    
        showCountdownMessage()
        
        isGameReset = true
        wasRewardWatched = false
        rewardAdSeen = .none
        board.hideLines(hide: false)

        Timer.scheduledTimer(withTimeInterval: Constants.waitToStart, repeats: false){ _ in
            self.isGameReset = false
            self.generateMarbles()
        }
        
        tierTimer?.invalidate()
        tierTimer = Timer.scheduledTimer(withTimeInterval: Constants.newTierTime, repeats: false){ _ in
            self.updateTier()
        }
        
        board.clearLines()
        destroyAllMarbles()
        
        self.updateGameState()
    }
    
    private func initNotifications(){
        NotificationCenter.default.addObserver(self, selector: #selector(self.resetGame), name: Notification.Name("RestartGame"), object: nil)
    }

    private func generateMarbles(){
        if (isGameReset || isTutorialShowing()){
            return
        }
        
        marbles.append(generateMarble())
        
        Timer.scheduledTimer(withTimeInterval: Globals.marbleGenerateWaitTime, repeats: false){ _ in
            self.generateMarbles()
        }
    }
    
    private func updateTier(){
        if (lives.isEmpty() || isGameReset || isTutorialShowing()){
            return
        }
        
        levelCounter.updateNewLevel()
        Globals.marbleTime = Globals.marbleTime * pow(Constants.marbleTimeDecrease, 150)
        StatisticsService.setHighestTier(tier: levelCounter.getLevel())
        Sounds.play(sound: Sounds.newTier, soundType: .sound)
        
        if (isPointedTiersEnabled){
            let pointsToAdd = 50
            score.addScore(amount: pointsToAdd)
            showScoreAdd(scoreAdded: pointsToAdd, frame: CGRect(x: gameView.frame.width/2, y: gameView.frame.height, width: 0, height: 0))
        }
        
        tierTimer = Timer.scheduledTimer(withTimeInterval: Constants.newTierTime, repeats: false){ _ in
            self.updateTier()
        }
    }
    
    private func generateMarble() -> Marble{
        let marbleWidth: CGFloat = gameView.frame.width/7.5
        let marbleHeight: CGFloat = marbleWidth
        
        let range = UInt32(fullView.frame.width - marbleWidth - Constants.boardPadding * 2)
        let randomX = CGFloat(arc4random_uniform(range)) + Constants.boardPadding
        let marbleStartFrame = CGRect(x: randomX, y: -marbleHeight, width: marbleWidth, height: marbleHeight)
        
        var marble: Marble!
        
        let curScore = score.getScore()
        let scoreRequired = 100
        
        var odds: UInt32 = 15
        if (curScore >= scoreRequired){
            odds = odds + 1
        }
        if (isOrangeMarbleEnabled){
            odds = odds + 2
        }
        
        let randomMarbleId = arc4random_uniform(odds)
        
        if (randomMarbleId < 6){
            marble = GreenMarble(frame: marbleStartFrame)
        }
        else if (randomMarbleId < 13){
            marble = RedMarble(frame: marbleStartFrame)
        }
        else if (randomMarbleId < 15){
            marble = BlueMarble(frame: marbleStartFrame)
        }
        else if (isOrangeMarbleEnabled){
            if (curScore < scoreRequired){
                marble = OrangeMarble(frame: marbleStartFrame)
            }
            else if (randomMarbleId == 15){
                marble = pickSpecialMarble(marbleStartFrame: marbleStartFrame)
            }
            else{
                marble = OrangeMarble(frame: marbleStartFrame)
            }
        }
        else{
            marble = pickSpecialMarble(marbleStartFrame: marbleStartFrame)
        }
        
        if (marble.isBomb() || marble.isHeart()){
            marble.frame.size = CGSize(width: marble.frame.width * 1.1, height: marble.frame.height * 1.1)
        }
        
        if (isGlowingMarblesEnabled){
            marble.layer.shadowOffset = .zero
            marble.layer.shadowColor = marble.getColor().cgColor
            marble.layer.shadowRadius = marbleWidth/5
            marble.layer.shadowOpacity = 0.8
            marble.layer.shadowPath = UIBezierPath(rect: marble.bounds).cgPath
        }
        
        Timer.scheduledTimer(withTimeInterval: 0.005, repeats: false){ _ in
            marble.startMoving()
        }
        
        gameView.addSubview(marble)
        return marble
    }
    
    private func pickSpecialMarble(marbleStartFrame: CGRect) -> Marble{
        let livesRemaining = lives.getRemainingLives()
        
        if (livesRemaining < 5 && !isNoBombsEnabled){
            return (arc4random_uniform(2) == 0) ? GrayMarble(frame: marbleStartFrame) : HeartMarble(frame: marbleStartFrame)
        }
        else if (livesRemaining < 5){
            return HeartMarble(frame: marbleStartFrame)
        }
        
        return GreenMarble(frame: marbleStartFrame)
    }
    
    private func updateGameState(){
        if (lives.isEmpty()){
            endGame()
            return
        }
        
        let minMarbleTime = 1.825
        
        if (Globals.marbleTime > minMarbleTime){
            Globals.marbleTime =  Globals.marbleTime * Constants.marbleTimeDecrease
        }
        let marbleVelocity = Double(Constants.marbleDistance)/Globals.marbleTime
        let timeToPassScreen = Double(gameView.frame.height)/marbleVelocity
        
        if (marblesPerScreen > 1 && Globals.marbleTime > minMarbleTime){
            marblesPerScreen = marblesPerScreen - 0.0005
        }
        
        Globals.marbleGenerateWaitTime = timeToPassScreen/marblesPerScreen
        
        var i = 0
        for marble in marbles{
            let marbleFrame = marble.getPresentationLayer()
            if (marbleFrame.midY > gameView.frame.height){
                if (marbleFrame.midY < Constants.marbleDistance){
                    marblePassed(marble: marble)
                }
                marble.removeFromSuperview()
                marbles.remove(at: i)
                break
            }
            i = i + 1
        }
        
        if (lastStreakAmount != Globals.streak){
            lastStreakAmount = Globals.streak
            if (Globals.streak > 0 && Globals.streak % 5 == 0){
                showStreakMessage()
                Sounds.play(sound: Sounds.streak, soundType: .sound)
            }
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01){
            self.updateGameState()
        }
    }
    
    private func marblePassed(marble: Marble){
        if (isGameReset || lives.isEmpty()){
            return
        }
        
        let marbleFrame = marble.getPresentationLayer()
        
        if (marble.isBad()){
            var livesLost  = marble.getLivesLost()
            if (marble.isBomb()){
                livesLost = lives.getRemainingLives()
            }
            showLivesRemoval(livesLost: livesLost, frame: marbleFrame)
            lives.removeLives(amount: livesLost, isBomb: marble.isBomb())
            Globals.streak = 0
            score.updateLabel()
        }
        else if (marble.isGood()){
            showScoreAdd(scoreAdded: marble.getScore(), frame: marbleFrame)
            score.addScore(amount: marble.getScore())
            Globals.streak = Globals.streak + 1
        }
    }

    private func endGame(){
        if (rewardAdSeen == .none){
            StatisticsService.setGamesPlayed()
            
            if (AdService.canShowAd()){
                if (interstitial.isReady){
                    Sounds.stopAllMusic()
                    interstitial.present(fromRootViewController: gameController)
                }
            }
            AdService.incrementShowAd()
        }
        StatisticsService.setBadMarblesHit(badMarblesHit: badMarblesHit)
        
        gameOverPopup.showPopup(score: score.getScore(), rewardAdSeen: rewardAdSeen, is2xExtraChanceEnabled: is2xExtraChanceEnabled)
        AchievementsService.reloadAchievements()
        logGamePlayedEvent()
    }

    private func showLivesRemoval(livesLost: Int, frame: CGRect){
        var floatText = "-"
        for _ in 1...livesLost{
            floatText = floatText + "♥"
        }
        showFloatText(text: floatText, color: UIColor.red, frame: frame)
    }
    
    private func showScoreAdd(scoreAdded: Int, frame: CGRect){
        let floatText = "+" + scoreAdded.format()
        showFloatText(text: floatText, color: UIColor.green, frame: frame)
    }
    
    private func showFloatText(text: String, color: UIColor, frame: CGRect){
        let floatText = FloatingText()
        floatText.frame = CGRect(x: frame.midX - 75, y: frame.minY - 25, width: 150, height: 50)
        floatText.text = text
        floatText.textColor = color
        gameView.addSubview(floatText)
        floatText.startFloating()
    }
    
    private func showCountdownMessage(){
        let timeBetweenText = 1.1
        
        let countdownText = createCenteredFancyText()
        countdownText.text = countDownSecs == 0 ? "START" : String(describing: Int(countDownSecs))
        countdownText.textColor = UIColor.white
        gameView.addSubview(countdownText)
        countdownText.startAnimating(duration: timeBetweenText)
        
        if (countDownSecs > 0){
            DispatchQueue.main.asyncAfter(deadline: .now() + timeBetweenText){
                self.showCountdownMessage()
            }
        }
        else if (countDownSecs == 0){
            Sounds.play(sound: Sounds.gameStart, soundType: .sound)
        }
        
        countDownSecs = countDownSecs - 1
    }
    
    private func showStreakMessage(){
        let streakText = createCenteredFancyText()
        let streak = 1 + (Globals.streak / 5)
        streakText.text = streak.format() + "X STREAK"
        streakText.textColor = getStreakColor(streak: streak)
        gameView.addSubview(streakText)
        streakText.startAnimating()
        
        StatisticsService.setBiggestStreak(streak: streak)
    }
    
    private func isTutorialShowing() -> Bool{
        return tutorialPopup.isShowing() || orangeMarbleTutorialPopup.isShowing()
    }
    
    private func destroyAllMarbles(){
        marbles.forEach({$0.destroy()})
        marbles.removeAll()
    }
    
    private func continueGameFromReward(){
        lives.resetLives()
        isGameReset = false
        tierTimer = Timer.scheduledTimer(withTimeInterval: Constants.newTierTime, repeats: false){ _ in
            self.updateTier()
        }
        Timer.scheduledTimer(withTimeInterval: 1.5, repeats: false){ _ in
            self.updateGameState()
            self.generateMarbles()
        }
        
        Sounds.play(sound: Sounds.gameStart, soundType: .sound)
        
        let extraChanceText = createCenteredFancyText(withExtraPadding: false)
        extraChanceText.text = "EXTRA CHANCE"
        extraChanceText.textColor = rewardAdSeen == .once ? Colors.scoreGreen : Colors.lighterBadMarblesBlue
        extraChanceText.startAnimating()
        gameView.addSubview(extraChanceText)
    }
    
    private func createCenteredFancyText(withExtraPadding: Bool = true) -> FancyText{
        let width: CGFloat = board.getBoard().frame.width - (withExtraPadding ? 50 : 15)
        let height: CGFloat = 200
        
        let fancyText = FancyText()
        fancyText.frame = CGRect(x: (gameView.frame.width - width)/2, y: (gameView.frame.height - height)/2, width: width, height: height)
        return fancyText
    }
    
    private func getNewRewardAdSeen() -> RewardAdSeen{
        if (rewardAdSeen == .none){
            return .once
        }
        if (rewardAdSeen == .once && is2xExtraChanceEnabled){
            return .twice
        }
        
        print("getNewRewardAdSeen() - This should never happen!")
        return rewardAdSeen
    }
    
    private func logGamePlayedEvent() {
        let event = AppEvent(name: "Game Played")
        AppEventsLogger.log(event)
    }
    
    private func getStreakColor(streak: Int) -> UIColor{
        switch streak{
        case 2:
            return UIColor.white
        case 3:
            return UIColor.green
        case 4:
            return UIColor.yellow
        default:
            return UIColor.orange
        }
    }
}
