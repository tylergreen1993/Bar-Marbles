//
//  SettingsController.swift
//  Bad Marbles
//
//  Created by Tyler Green on 6/1/18.
//  Copyright © 2018 Tyler Green. All rights reserved.
//

import UIKit

class SettingsController: UIViewController {
    @IBOutlet weak var darkerImageBackground: UIImageView!
    @IBOutlet weak var backArrow: BackImage!
    @IBOutlet weak var settingsLabel: UILabel!
    @IBOutlet weak var musicLabel: UILabel!
    @IBOutlet weak var soundEffectsLabel: UILabel!
    @IBOutlet weak var musicSwitch: UISwitch!
    @IBOutlet weak var soundEffectsSwitch: UISwitch!
    @IBOutlet weak var tipTheCreatorButton: UIButton!
    @IBOutlet weak var statisticsButton: UIButton!
    @IBOutlet weak var showTutorialButton: UIButton!
    @IBOutlet weak var creditsButton: UIButton!
    @IBOutlet weak var restorePurchasesButton: UIButton!
    
    var tutorialPopup: TutorialPopup!
    var orangeMarbleTutorialPopup: OrangeMarbleTutorialPopup!
    var tipPopup: TipPopup!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tutorialPopup = TutorialPopup(fullView: view, demo: true)
        orangeMarbleTutorialPopup = OrangeMarbleTutorialPopup(fullView: view, demo: true)
        tipPopup = TipPopup(fullView: view)
        
        musicSwitch.isOn = SoundService.getMusicEnabled()
        soundEffectsSwitch.isOn = SoundService.getSoundEnabled()
        
        Sounds.play(sound: Sounds.woosh, soundType: .sound)
        
        GradientService.addGradient(view: view, firstColor: Colors.softPurple, secondColor: Colors.darkPurple)
        
        initSizes()
    }

    func initSizes(){
        let padding: CGFloat = 15
        
        darkerImageBackground.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: darkerImageBackground.frame.height)
        settingsLabel.frame = CGRect(x: 0, y: settingsLabel.frame.origin.y, width: view.frame.width, height: settingsLabel.frame.height)
        musicSwitch.frame.origin = CGPoint(x: view.frame.width - musicSwitch.frame.width - padding, y: musicLabel.frame.origin.y)
        soundEffectsSwitch.frame.origin = CGPoint(x: view.frame.width - soundEffectsSwitch.frame.width - padding, y: soundEffectsLabel.frame.origin.y)
        
        settingsLabel.addShadow()
    }
    
    @IBAction func musicSwitched(_ sender: UISwitch) {
        SoundService.toggleMusic()
        Sounds.play(sound: Sounds.menuMusic, soundType: .music)
    }
    
    @IBAction func soundEffectsSwitched(_ sender: UISwitch) {
        SoundService.toggleSound()
    }
    
    @IBAction func tipTheCreatorPressed(_ sender: UIButton) {
        tipPopup.showPopup()
    }
    
    @IBAction func tutorialButtonPressed(_ sender: UIButton) {
        tutorialPopup.forcePopup()
        orangeMarbleTutorialPopup.forcePopup()
    }
    
    @IBAction func statisticsButtonPressed(_ sender: UIButton) {
        StatisticsService.showInfoPopup()
    }
    
    @IBAction func creditsButtonPressed(_ sender: UIButton) {
        let creditMessage = "\nProgramming, design, and music by Tyler Green (www.tylerlgreen.com).\n\nSome sounds used with permission from Zapsplat.\n\nSome images used with permission from Pixabay."
        
        AlertService.showAlert(title: "Credits", message: creditMessage)
    }
    
    @IBAction func restorePurchasesButtonPressed(_ sender: UIButton) {
        IAPService.shared.restorePurchases()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}
