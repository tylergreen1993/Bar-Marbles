//
//  MainController.swift
//  Bad Marbles
//
//  Created by Tyler Green on 5/30/18.
//  Copyright © 2018 Tyler Green. All rights reserved.
//

import UIKit

class MainController: UIViewController {

    @IBOutlet weak var shareIcon: ShareImage!
    @IBOutlet weak var marblesTrophy: Trophy!
    @IBOutlet weak var highScoreLabel: UILabel!
    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var playGameButton: UIButton!
    @IBOutlet weak var unlockablesButton: BadgeButton!
    @IBOutlet weak var settingsButton: UIButton!
    @IBOutlet weak var copyrightLabel: UILabel!
    @IBOutlet weak var cloud: Cloud!
    @IBOutlet weak var achievementTrophy: AchievementIcon!
    
    let unlockablesRepository = UnlockablesRepository()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initText()
        initSizes()
        cloud.startMoving()
        shareIcon.setViewController(viewController: self)
        achievementTrophy.initPopup(fullView: view)
        
        GradientService.addGradient(view: view, firstColor: Colors.badMarblesBlue, secondColor: Colors.lighterBadMarblesBlue)
        IAPService.shared.fetchAvailableProducts()
        TipService.shared.updateTipPrices()
    }
    
    func initText(){
        let highScore = ScoreService.getHighScore()
        highScoreLabel.attributedText = getHighScoreText(highScore: highScore)
        copyrightLabel.text = "Bad Marbles © " + getCurrentYear() + " V." + getCurrentVersion()
    }
    
    func initSizes(){
        let buttonHeight = view.frame.height/10.5
        let buttonWidth = view.frame.width - 145
        let totalItems: CGFloat = 4
        
        let logoWidth: CGFloat = view.frame.width - 10
        let logoHeight: CGFloat = logoWidth/1.89
        logo.frame = CGRect(x: view.frame.midX - logoWidth/2, y: 30, width: logoWidth, height: logoHeight)
        cloud.frame.size = CGSize(width: logoWidth/2, height: logoHeight/2)
        let marblesTrophyWidth = view.frame.width/4.5
        marblesTrophy.frame.size = CGSize(width: marblesTrophyWidth, height: marblesTrophyWidth * 1.15)
        
        let buttonSpacing = (view.frame.height - logo.frame.height - buttonHeight * totalItems - 85)/totalItems
        
        let highScoreLabelPadding: CGFloat = 160
        highScoreLabel.frame = CGRect(x: highScoreLabelPadding/2, y: logo.frame.maxY + 20, width: view.frame.width - highScoreLabelPadding, height: buttonHeight)
        playGameButton.frame = CGRect(x: (view.frame.width - buttonWidth)/2, y: highScoreLabel.frame.maxY + buttonSpacing - 5, width: buttonWidth, height: buttonHeight)
        unlockablesButton.frame = CGRect(x: (view.frame.width - buttonWidth)/2, y: playGameButton.frame.maxY + buttonSpacing, width: buttonWidth, height: buttonHeight)
        settingsButton.frame = CGRect(x: (view.frame.width - buttonWidth)/2, y: unlockablesButton.frame.maxY + buttonSpacing, width: buttonWidth, height: buttonHeight)
        copyrightLabel.frame = CGRect(x: 70, y: settingsButton.frame.maxY + 2 * buttonSpacing/3 + 5, width: view.frame.width - 140, height: copyrightLabel.frame.height)
        let shareIconWidth: CGFloat = min(view.frame.width/10, 50)
        let shareIconHeight =  shareIconWidth/1.2
        shareIcon.frame = CGRect(x: 15, y: copyrightLabel.frame.midY - shareIconHeight/2, width: shareIconWidth, height: shareIconHeight)
        let achievementTrophyWidth: CGFloat = min(view.frame.width/12, 45)
        let achievementTrophyHeight =  achievementTrophyWidth * 1.15
        achievementTrophy.frame = CGRect(x: view.frame.width - achievementTrophyWidth - 15, y: copyrightLabel.frame.midY - achievementTrophyHeight/2, width: achievementTrophyWidth, height: achievementTrophyHeight)
        
        playGameButton.fitTextToButton()
        unlockablesButton.fitTextToButton()
        settingsButton.fitTextToButton()
        highScoreLabel.adjustsFontSizeToFitWidth = true
        highScoreLabel.baselineAdjustment = .alignCenters
        copyrightLabel.adjustsFontSizeToFitWidth = true
        highScoreLabel.addShadow()
        copyrightLabel.addShadow()
    }
    
    func getCurrentYear() -> String{
        let calendar = NSCalendar.init(calendarIdentifier: NSCalendar.Identifier.gregorian)
        if let year = calendar?.component(NSCalendar.Unit.year, from: Date()){
            return String(describing: year)
        }
        
        return ""
    }
    
    func getCurrentVersion() -> String{
        if let curVersion = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String{
            return curVersion
        }
        
        return ""
    }
    
    func addBadgeToUnlockablesButton(){
        let prevUnlockablesCount = unlockablesRepository.getUnlockablesByMaxPoints(points: ScoreService.getLastCheckedTotalPointsUnlockables()).count
        let newUnlockablesCount = unlockablesRepository.getUnlockablesByMaxPoints(points: ScoreService.getTotalPoints()).count
        
        if (newUnlockablesCount > prevUnlockablesCount){
            unlockablesButton.badge = "+" + (newUnlockablesCount - prevUnlockablesCount).format()
        }
        else{
            unlockablesButton.removeBadge()
        }
    }
    
    func getHighScoreText(highScore: Int) -> NSAttributedString{
        let scoreAttribute = [NSAttributedStringKey.foregroundColor: Colors.scoreGreen]
        let highScoreText =  NSMutableAttributedString(string: "High Score: ")
        let scoreText = NSMutableAttributedString(string: highScore.format(), attributes: scoreAttribute)
        
        highScoreText.append(scoreText)
        
        return highScoreText
    }
    
    override func viewDidAppear(_ animated: Bool){
        initText()
        addBadgeToUnlockablesButton()
        marblesTrophy.checkIfEnabled()
        achievementTrophy.updateImage()
        ReviewService.requestReview()
        
        logo.image = MiscUnlockableService.isSecretLogoEnabled() ? #imageLiteral(resourceName: "Bad Marbles Logo Gold") : #imageLiteral(resourceName: "Bad Marbles Logo")
        
        if let menuMusic = Sounds.menuMusic{
            if (!menuMusic.playing){
                Sounds.stopAllMusic()
                Sounds.play(sound: menuMusic, soundType: .music)
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}
