//
//  MainController.swift
//  Bad Marbles
//
//  Created by Tyler Green on 4/21/18.
//  Copyright © 2018 Tyler Green. All rights reserved.
//

import UIKit

class GameController: UIViewController {
    
    @IBOutlet weak var gameBackground: UIImageView!
    @IBOutlet weak var gameView: UIView!
    @IBOutlet weak var scoreView: ScoreView!
    
    var gameEngine: GameEngine!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Sounds.stopAllMusic()
        Sounds.play(sound: BackgroundMusicService.getSelectedBackgroundMusic(), soundType: .music)
        
        setGameBackground()
        
        gameBackground.frame = CGRect(x: -30, y: -30, width: view.frame.width * 1.1, height: view.frame.height * 1.1)
        scoreView.frame = CGRect(x: 0, y: -Constants.scoreViewStartHeight, width: view.frame.width, height: Constants.scoreViewHeight)
        gameView.frame = CGRect(x: 0, y: scoreView.frame.maxY, width: view.frame.width, height: view.frame.height - scoreView.frame.height + Constants.scoreViewStartHeight)
        
        gameEngine =  GameEngine(fullView: view, gameView: gameView, scoreView: scoreView, gameController: self)
        gameEngine.startGame()
    }

    func setGameBackground(){
        gameBackground.image = BackgroundAppearanceService.getSelectedBackground()
        gameBackground.layer.minificationFilter = kCAFilterTrilinear
        gameBackground.contentMode = .scaleAspectFill
        gameBackground.addParallax()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}

