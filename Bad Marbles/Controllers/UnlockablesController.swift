//
//  UnlockablesController.swift
//  Bad Marbles
//
//  Created by Tyler Green on 6/1/18.
//  Copyright © 2018 Tyler Green. All rights reserved.
//

import UIKit

class UnlockablesController: UIViewController {
    @IBOutlet weak var unlockablesLabel: UILabel!
    @IBOutlet weak var darkerImageBackground: UIImageView!
    @IBOutlet weak var unlockablesScrollview: UIScrollView!
    
    var unlockablesEngine: UnlockablesEngine!
    
    var totalPoints = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Sounds.play(sound: Sounds.woosh, soundType: .sound)
        
        totalPoints = ScoreService.getTotalPoints()
        
        initLabel()
        initSizes()
        
        GradientService.addGradient(view: view, firstColor: Colors.unlockablesPink, secondColor: Colors.softPurple)
        
        unlockablesEngine = UnlockablesEngine(fullView: view, scrollView: unlockablesScrollview, totalPoints: totalPoints)
        unlockablesEngine.populateUnlockables()
    }
    
    func initLabel(){
        let unlockablesText =  NSMutableAttributedString(string: "Unlockables")
        let totalPointsText = getTotalPointsText(getTotalPoints: totalPoints)
        
        unlockablesText.append(totalPointsText)
        
        unlockablesLabel.font = UIFont.systemFont(ofSize: 30, weight: .semibold)
        unlockablesLabel.attributedText = unlockablesText
        unlockablesLabel.addShadow()
    }
    
    func initSizes(){
        darkerImageBackground.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: darkerImageBackground.frame.height)
        unlockablesLabel.frame = CGRect(x: 0, y: unlockablesLabel.frame.origin.y, width: view.frame.width, height: unlockablesLabel.frame.height)
        unlockablesScrollview.frame = CGRect(x: 0, y: darkerImageBackground.frame.maxY, width: view.frame.width, height: view.frame.height)
    }
    
    func getTotalPointsText(getTotalPoints: Int) -> NSAttributedString{
        let textAttribute = [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 17, weight: .semibold)]
        let scoreAttribute = [NSAttributedStringKey.foregroundColor: Colors.scoreGreen, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 17, weight: .bold)]
        let highScoreText =  NSMutableAttributedString(string: "\nTotal Points: ", attributes: textAttribute)
        let scoreText = NSMutableAttributedString(string: getTotalPoints.format(), attributes: scoreAttribute)
        
        highScoreText.append(scoreText)
        
        return highScoreText
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}
