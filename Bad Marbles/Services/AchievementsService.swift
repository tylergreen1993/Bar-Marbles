//
//  AchievementsService.swift
//  Bad Marbles
//
//  Created by Tyler Green on 10/26/18.
//  Copyright © 2018 Tyler Green. All rights reserved.
//

import UIKit

class AchievementsService{
    static let lastSeenCountKey = "LastSeenAchievementsCount"
    static let achievementsRepository = AchievementsRepository()
    
    static func getLastSeenUnlockedCount() -> Int{
        if (UserDefaults.standard.object(forKey: lastSeenCountKey) == nil){
            return 0
        }
        
        return UserDefaults.standard.integer(forKey: lastSeenCountKey)
    }
    
    static func updateLastSeenUnlockedCount(){
        let unlockedCount = getAchievements().filter({$0.ratio >= 1}).count
        if (unlockedCount > getLastSeenUnlockedCount()){
            UserDefaults.standard.set(unlockedCount, forKey: lastSeenCountKey)
        }
    }
    
    static func reloadAchievements(){
        achievementsRepository.reloadAchievements()
    }
    
    static func getAchievements() -> [AchievementDetails]{
        return achievementsRepository.getAchievements()
    }
}
