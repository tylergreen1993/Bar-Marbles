//
//  AdService.swift
//  Bad Marbles
//
//  Created by Tyler Green on 7/10/18.
//  Copyright © 2018 Tyler Green. All rights reserved.
//

import UIKit
import GoogleMobileAds

class AdService{
    static let adsSeenKey = "AdsSeenCount"
    static let interstitialAdUnitId = "ca-app-pub-9811434783745638/7762462229"
    static let rewardAdUnitId = "ca-app-pub-9811434783745638/5813325737"
    
    static func createAndLoadInterstitial(controller: GADInterstitialDelegate) -> GADInterstitial {
        let interstitial = GADInterstitial(adUnitID: interstitialAdUnitId)
        interstitial.delegate = controller
        interstitial.load(GADRequest())
        
        let request = GADRequest()
        request.testDevices = [kGADSimulatorID, "ba708867a662d469d438eaa3f816e6d1"]
        interstitial.load(request)
        
        return interstitial
    }
    
    static func canShowAd() -> Bool{
        if (MiscUnlockableService.isNoAdsEnabled()){
            return false
        }
        
        var adsSeen = 0
        
        if (UserDefaults.standard.object(forKey: adsSeenKey) != nil){
            adsSeen = UserDefaults.standard.integer(forKey: adsSeenKey)
        }
        
        if (adsSeen >= 2){
            return true
        }
        
        return false
    }
    
    static func incrementShowAd(){
        var adsSeen = 0
        
        if (UserDefaults.standard.object(forKey: adsSeenKey) != nil){
            adsSeen = UserDefaults.standard.integer(forKey: adsSeenKey)
        }
        
        if (adsSeen >= 2){
            adsSeen = 0
        }
        else{
            adsSeen = adsSeen + 1
        }

        UserDefaults.standard.set(adsSeen, forKey: adsSeenKey)
    }
    
    static func initRewardAds(controller: GADRewardBasedVideoAdDelegate){
        GADRewardBasedVideoAd.sharedInstance().delegate = controller
        loadRewardAd()
    }
    
    static func loadRewardAd(){
        GADRewardBasedVideoAd.sharedInstance().load(GADRequest(), withAdUnitID: rewardAdUnitId)
    }
}

enum RewardAdSeen{
    case none
    case once
    case twice
}
