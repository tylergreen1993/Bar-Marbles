//
//  HighScoreService.swift
//  Bad Marbles
//
//  Created by Tyler Green on 5/21/18.
//  Copyright © 2018 Tyler Green. All rights reserved.
//

import UIKit

class ScoreService{
    static let highScoreKey = "HighScore"
    static let totalPointsKey = "TotalPoints"
    static let lastTotalPointsUnlockablesKey = "LastTotalPointsUnlockables"
    static let lastTotalPointsKey = "LastTotalPoints"
    
    static func setHighScore(score: Int){
        let curHighScore = getHighScore()
        
        if (score > curHighScore){
            UserDefaults.standard.set(score, forKey: highScoreKey)
            Sounds.play(sound: Sounds.newHighScore, soundType: .sound)
        }
    }
    
    static func getHighScore() -> Int{
        if (UserDefaults.standard.object(forKey: highScoreKey) == nil){
            return 0
        }
        
        return UserDefaults.standard.integer(forKey: highScoreKey)
    }
    
    static func setTotalPoints(score: Int){
        let curTotalPoints = getTotalPoints()
        
        UserDefaults.standard.set(score + curTotalPoints, forKey: totalPointsKey)
        UserDefaults.standard.set(curTotalPoints, forKey: lastTotalPointsKey)
    }
    
    static func getTotalPoints() -> Int{
        if (UserDefaults.standard.object(forKey: totalPointsKey) == nil){
            return 0
        }
        
        return UserDefaults.standard.integer(forKey: totalPointsKey)
    }
    
    static func getLastCheckedTotalPoints() -> Int{
        if (UserDefaults.standard.object(forKey: lastTotalPointsKey) == nil){
            return 0
        }
        
        return UserDefaults.standard.integer(forKey: lastTotalPointsKey)
    }
    
    static func getLastCheckedTotalPointsUnlockables() -> Int{
        if (UserDefaults.standard.object(forKey: lastTotalPointsUnlockablesKey) == nil){
            return 0
        }
        
        return UserDefaults.standard.integer(forKey: lastTotalPointsUnlockablesKey)
    }
    
    static func setLastCheckedTotalPoints(){
        UserDefaults.standard.set(getTotalPoints(), forKey: lastTotalPointsKey)
    }
    
    static func setLastCheckedTotalPointsUnlockables(){
        UserDefaults.standard.set(getTotalPoints(), forKey: lastTotalPointsUnlockablesKey)
    }
}
