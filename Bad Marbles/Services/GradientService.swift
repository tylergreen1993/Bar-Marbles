//
//  GradientService.swift
//  Bad Marbles
//
//  Created by Tyler Green on 7/18/18.
//  Copyright © 2018 Tyler Green. All rights reserved.
//

import UIKit

class GradientService{
    static func addGradient(view: UIView, firstColor: UIColor, secondColor: UIColor){
        let gradient = CAGradientLayer()
        gradient.frame = view.bounds
        gradient.colors = [firstColor.cgColor, secondColor.cgColor]
        
        view.layer.insertSublayer(gradient, at: 0)
    }
}
