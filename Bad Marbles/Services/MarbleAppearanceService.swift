//
//  MarbleAppearanceService.swift
//  Bad Marbles
//
//  Created by Tyler Green on 6/27/18.
//  Copyright © 2018 Tyler Green. All rights reserved.
//

import UIKit

class MarbleAppearanceService{
    static let selectedIdKey = "SelectedMarbleAppearanceId"
    static let marbleIdList = [MarbleId(green: #imageLiteral(resourceName: "Green Marble"), red: #imageLiteral(resourceName: "Red Marble"), blue: #imageLiteral(resourceName: "Blue Marble"), orange: #imageLiteral(resourceName: "Orange Marble"), id: 5),
                               MarbleId(green: #imageLiteral(resourceName: "Green Food"), red: #imageLiteral(resourceName: "Red Food"), blue: #imageLiteral(resourceName: "Blue Food"), orange: #imageLiteral(resourceName: "Orange Food"), id: 6),
                               MarbleId(green: #imageLiteral(resourceName: "Green Emoji"), red: #imageLiteral(resourceName: "Red Emoji"), blue: #imageLiteral(resourceName: "Blue Emoji"), orange: #imageLiteral(resourceName: "Orange Emoji"), id: 13),
                               MarbleId(green: #imageLiteral(resourceName: "Green Sports"), red: #imageLiteral(resourceName: "Red Sports"), blue: #imageLiteral(resourceName: "Blue Sports"), orange: #imageLiteral(resourceName: "Orange Sports"), id: 17),
                               MarbleId(green: #imageLiteral(resourceName: "Green Outline"), red: #imageLiteral(resourceName: "Red Outline"), blue: #imageLiteral(resourceName: "Blue Outline"), orange: #imageLiteral(resourceName: "Orange Outline"), id: 19),
                               MarbleId(green: #imageLiteral(resourceName: "Green Casino"), red: #imageLiteral(resourceName: "Red Casino"), blue: #imageLiteral(resourceName: "Blue Casino"), orange: #imageLiteral(resourceName: "Orange Casino"), id: 22),
                               MarbleId(green: #imageLiteral(resourceName: "Green Secret"), red: #imageLiteral(resourceName: "Red Secret"), blue: #imageLiteral(resourceName: "Blue Secret"), orange: #imageLiteral(resourceName: "Orange Secret"), id: 29)]
    
    static func getSelectedId() -> Int{
        if (UserDefaults.standard.object(forKey: selectedIdKey) == nil){
            return marbleIdList[0].id
        }
        
        return UserDefaults.standard.integer(forKey: selectedIdKey)
    }
    
    static func getMarble(color: MarbleColor) -> UIImage{
        if let marble = marbleIdList.filter({$0.id == getSelectedId()}).first{
            switch color{
            case .red:
                return marble.red
            case .blue:
                return marble.blue
            case .orange:
                return marble.orange
            default:
                return marble.green
            }
        }
        
        return UIImage()
    }
    
    static func setMarbleAppearance(id: Int){
        if (id != getSelectedId() && marbleIdList.filter({$0.id == id}).count > 0){
            UserDefaults.standard.set(id, forKey: selectedIdKey)
        }
    }
}

struct MarbleId{
    var green: UIImage
    var red: UIImage
    var blue: UIImage
    var orange: UIImage
    var id: Int
}

enum MarbleColor{
    case green, red, blue, orange
}

