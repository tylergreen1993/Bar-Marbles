//
//  TipService.swift
//  Bad Marbles
//
//  Created by Tyler Green on 10/8/18.
//  Copyright © 2018 Tyler Green. All rights reserved.
//

import UIKit
import StoreKit

class TipService: NSObject, SKProductsRequestDelegate, SKPaymentTransactionObserver{
    static let shared = TipService()
    private let totalAmountTippedKey = "TotalAmountTipped"
    private var tipItems = [TipItem(image: #imageLiteral(resourceName: "Coins"), name: "Spare Change", price: "Tip", purchaseId: "TipSmall"),
                           TipItem(image: #imageLiteral(resourceName: "Coffee"), name: "Cup of Coffee", price: "Tip", purchaseId: "TipMedium"),
                           TipItem(image: #imageLiteral(resourceName: "Popcorn"), name: "Snack", price: "Tip", purchaseId: "TipLarge"),
                           TipItem(image: #imageLiteral(resourceName: "Sandwich"), name: "Lunch", price: "Tip", purchaseId: "TipExtraLarge")]
    private var iapProducts = [SKProduct]()
    private var productID = ""
    
    func getTipItems() -> [TipItem]{
        return tipItems
    }
    
   func updateTipPrices(){
        let tipIds = tipItems.filter({!$0.purchaseId.isEmpty}).map({$0.purchaseId})
        let productIdentifiers = Set(tipIds)
        
        let productsRequest = SKProductsRequest(productIdentifiers: productIdentifiers)
        productsRequest.delegate = self
        productsRequest.start()
    }
    
    func purchaseProduct(productId: String){
        if (iapProducts.count == 0) {
            AlertService.showAlert(title: "Tip Failed", message: "Could not tip. Try again later.")
            return
        }
        
        if (SKPaymentQueue.canMakePayments()) {
            if let product = iapProducts.filter({$0.productIdentifier == productId}).first{
                productID = productId
                let payment = SKPayment(product: product)
                SKPaymentQueue.default().add(self)
                SKPaymentQueue.default().add(payment)
            }
            else{
                AlertService.showAlert(title: "Tip Failed", message: "Could not tip. Try again later.")
            }
        }
        else{
            AlertService.showAlert(title: "Tip Failed", message: "You must enable in-app purchases on your phone.")
        }
    }
    
    func productsRequest (_ request:SKProductsRequest, didReceive response:SKProductsResponse) {
        iapProducts = response.products
        for product in response.products{
            for tip in getTipItems(){
                if (product.productIdentifier == tip.purchaseId){
                    if let formattedPrice = product.formattedPrice(){
                        tip.price = formattedPrice
                    }
                }
            }
        }
    }
    
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        for transaction:AnyObject in transactions {
            if let trans = transaction as? SKPaymentTransaction {
                switch trans.transactionState {
                case .purchased:
                    SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                    Sounds.play(sound: Sounds.purchased, soundType: .sound)
                    AlertService.showAlert(title: "Tip Successful", message: "Thanks for the tip! It is very much appreciated. You're one of the good marbles!")
                    break
                case .failed:
                    SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                    AlertService.showAlert(title: "Tip Failed", message: "Could not tip. Try again later.")
                    break
                case .restored:
                    SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                    break
                default: break
                }
            }
        }
    }
}

class TipItem{
    var image: UIImage
    var name: String
    var price: String
    var purchaseId: String
    
    init(image: UIImage, name: String, price: String, purchaseId: String){
        self.image = image
        self.name = name
        self.price = price
        self.purchaseId = purchaseId
    }
}
