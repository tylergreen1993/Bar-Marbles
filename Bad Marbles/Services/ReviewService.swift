//
//  ReviewService.swift
//  Bad Marbles
//
//  Created by Tyler Green on 9/30/18.
//  Copyright © 2018 Tyler Green. All rights reserved.
//

import UIKit
import StoreKit

class ReviewService{
    static let reviewLastAskedDateKey = "ReviewLastAskedDate"
    
    static func requestReview(){
        let gamesPlayed = StatisticsService.getGamesPlayed()
        
        if (gamesPlayed < 5){
            return
        }
        
        if (UserDefaults.standard.object(forKey: reviewLastAskedDateKey) != nil){
            if let lastAskedDate = UserDefaults.standard.object(forKey: reviewLastAskedDateKey) as? Date{
                let currentDate = Date()
                if let days = Calendar.current.dateComponents([.day], from: lastAskedDate, to: currentDate).day{
                    if (days < 3){
                        return
                    }
                }
            }
        }
        
        SKStoreReviewController.requestReview()
        UserDefaults.standard.set(Date(), forKey: reviewLastAskedDateKey)
    }
}
