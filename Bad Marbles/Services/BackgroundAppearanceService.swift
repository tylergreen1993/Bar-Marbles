//
//  BackgroundAppearanceService.swift
//  Bad Marbles
//
//  Created by Tyler Green on 7/1/18.
//  Copyright © 2018 Tyler Green. All rights reserved.
//

import UIKit

class BackgroundAppearanceService{
    static let selectedIdKey = "SelectedBackgroundId"
    static let backgroundIdList = [BackgroundId(background: #imageLiteral(resourceName: "View Background"), id: 7),
                                   BackgroundId(background: #imageLiteral(resourceName: "Kaleidoscope Background"), id: 9),
                                   BackgroundId(background: #imageLiteral(resourceName: "Champion Background"), id: 10),
                                   BackgroundId(background: #imageLiteral(resourceName: "Space Background"), id: 16),
                                   BackgroundId(background: #imageLiteral(resourceName: "Bad Marbles Background"), id: 18),
                                   BackgroundId(background: #imageLiteral(resourceName: "Llama Background"), id: 23),
                                   BackgroundId(background: #imageLiteral(resourceName: "Scribbles Background"), id: 30)]
    
    static func getSelectedId() -> Int{
        if (UserDefaults.standard.object(forKey: selectedIdKey) == nil){
            return backgroundIdList[0].id
        }
        
        return UserDefaults.standard.integer(forKey: selectedIdKey)
    }
    
    static func getSelectedBackground() -> UIImage{
        let id = getSelectedId()
        if let background = backgroundIdList.filter({$0.id == id}).first?.background{
            return background
        }
        
        return backgroundIdList[0].background
    }
    
    static func setBackground(id: Int){
        if (id != getSelectedId() && backgroundIdList.filter({$0.id == id}).count > 0){
            UserDefaults.standard.set(id, forKey: selectedIdKey)
        }
    }
}

struct BackgroundId{
    var background: UIImage
    var id: Int
    
    init(background: UIImage, id: Int){
        self.background = background
        self.id = id
    }
}
