//
//  BackgroundMusicService.swift
//  Bad Marbles
//
//  Created by Tyler Green on 6/27/18.
//  Copyright © 2018 Tyler Green. All rights reserved.
//

import UIKit
import AudioToolbox

class BackgroundMusicService{
    static let selectedIdKey = "SelectedMusicId"
    static let musicIdList = [MusicId(music: Sounds.backgroundGameMusic, id: 2),
                              MusicId(music: Sounds.badMarblesTheme, id: 3),
                              MusicId(music: Sounds.badMarblesAcoustic, id: 4),
                              MusicId(music: Sounds.spokenWord, id: 11),
                              MusicId(music: Sounds.marblesAnthem, id: 14),
                              MusicId(music: Sounds.bohemianMarbles, id: 21),
                              MusicId(music: Sounds.secretTheme, id: 27)]
    
    static func getSelectedId() -> Int{
        if (UserDefaults.standard.object(forKey: selectedIdKey) == nil){
            return musicIdList[0].id
        }
        
        return UserDefaults.standard.integer(forKey: selectedIdKey)
    }
    
    static func getSelectedBackgroundMusic() -> Sound?{
        let id = getSelectedId()
        if let music = musicIdList.filter({$0.id == id}).first?.music{
            return music
        }
        
        return musicIdList[0].music
    }
    
    static func setBackgroundMusic(id: Int){
        if (id != getSelectedId() && musicIdList.filter({$0.id == id}).count > 0){
            UserDefaults.standard.set(id, forKey: selectedIdKey)
        }
    }
}

struct MusicId{
    var music: Sound?
    var id: Int
    
    init(music: Sound?, id: Int){
        self.music = music
        self.id = id
    }
}
