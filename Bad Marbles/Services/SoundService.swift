//
//  SoundService.swift
//  Bad Marbles
//
//  Created by Tyler Green on 6/3/18.
//  Copyright © 2018 Tyler Green. All rights reserved.
//

import UIKit

class SoundService{
    static let soundKey = "SoundEnabled"
    static let musicKey = "MusicEnabled"
    
    static func toggleMusic(){
        let musicEnabled = getMusicEnabled()
        
        if (musicEnabled){
            Sounds.stopAllMusic()
        }
        
        UserDefaults.standard.set(!musicEnabled, forKey: musicKey)
    }
    
    static func toggleSound(){
        let soundEnabled = getSoundEnabled()
        
        UserDefaults.standard.set(!soundEnabled, forKey: soundKey)
    }
    
    static func getSoundEnabled() -> Bool{
        if (UserDefaults.standard.object(forKey: soundKey) == nil){
            return true
        }
        
        return UserDefaults.standard.bool(forKey: soundKey)
    }
    
    static func getMusicEnabled() -> Bool{
        if (UserDefaults.standard.object(forKey: musicKey) == nil){
            return true
        }
        
        return UserDefaults.standard.bool(forKey: musicKey)
    }
}
