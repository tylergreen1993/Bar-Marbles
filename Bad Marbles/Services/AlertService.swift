//
//  AlertService.swift
//  Bad Marbles
//
//  Created by Tyler Green on 7/15/18.
//  Copyright © 2018 Tyler Green. All rights reserved.
//

import UIKit

class AlertService{
    static func showAlert(title: String, message: String){
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let closeAction = UIAlertAction(title: "Close", style: .cancel)
        alertController.addAction(closeAction)

        let alertWindow = UIWindow(frame: UIScreen.main.bounds)
        alertWindow.rootViewController = UIViewController()
        alertWindow.windowLevel = UIWindowLevelAlert + 1
        alertWindow.makeKeyAndVisible()
        
        alertWindow.rootViewController?.present(alertController, animated: true, completion: nil)
    }
}
