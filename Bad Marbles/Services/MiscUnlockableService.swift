//
//  MiscUnlockableService.swift
//  Bad Marbles
//
//  Created by Tyler Green on 7/1/18.
//  Copyright © 2018 Tyler Green. All rights reserved.
//

import UIKit

class MiscUnlockableService{
    static let selectedIdsKey = "SelectedMiscIds"
    
    static func getSelectedIds() -> [Int]{
        if (UserDefaults.standard.object(forKey: selectedIdsKey) == nil){
            return []
        }
        
        if let selectedIds = UserDefaults.standard.array(forKey: selectedIdsKey) as? [Int]{
            return selectedIds
        }
        
        return []
    }
    
    static func setUnlockable(id: Int){
        var selectedIds = getSelectedIds()
        if (!selectedIds.contains(id)){
            selectedIds.append(id)
            UserDefaults.standard.set(selectedIds, forKey: selectedIdsKey)
        }
    }
    
    static func removeUnlockable(id: Int){
        var selectedIds = getSelectedIds()
        for i in 0...selectedIds.count - 1{
            if (selectedIds[i] == id){
                selectedIds.remove(at: i)
                UserDefaults.standard.set(selectedIds, forKey: selectedIdsKey)
                return
            }
        }
    }
    
    static func checkForId(id: Int) -> Bool{
        for selectedId in getSelectedIds(){
            if (selectedId == id){
                return true
            }
        }
        
        return false
    }
    
    //unlockable checker methods
    
    static func isNoAdsEnabled() -> Bool{
        return checkForId(id: 0)
    }
    
    static func isExtraLifeEnabled() -> Bool{
        return checkForId(id: 1)
    }
    
    static func isOrangeMarbleEnabled() -> Bool{
        return checkForId(id: 8)
    }
    
    static func isGlowingMarblesEnabled() -> Bool{
        return checkForId(id: 12)
    }
    
    static func isMarblesTrophyEnabled() -> Bool{
        return checkForId(id: 15)
    }
    
    static func isNoBombsEnabled() -> Bool{
        return checkForId(id: 20)
    }
    
    static func isExtraLife2Enabled() -> Bool{
        return checkForId(id: 24)
    }
    
    static func isPointedTiersEnabled() -> Bool{
        return checkForId(id: 25)
    }
    
    static func is2xExtraChanceEnabled() -> Bool{
        return checkForId(id: 26)
    }
    
    static func isSecretLogoEnabled() -> Bool{
        return checkForId(id: 28)
    }
}
