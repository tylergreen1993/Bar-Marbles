//
//  StatisticsService.swift
//  Bad Marbles
//
//  Created by Tyler Green on 7/20/18.
//  Copyright © 2018 Tyler Green. All rights reserved.
//

import UIKit

class StatisticsService{
    static let biggestStreakKey = "LongestStreak"
    static let gamesPlayedKey = "GamesPlayed"
    static let badMarblesHitKey = "BadMarblesHit"
    static let highestTierKey = "HighestTier"
    
    static func getBiggestStreak() -> Int{
        if (UserDefaults.standard.object(forKey: biggestStreakKey) == nil){
            return 1
        }
        
        return UserDefaults.standard.integer(forKey: biggestStreakKey)
    }
    
    static func getGamesPlayed() -> Int{
        if (UserDefaults.standard.object(forKey: gamesPlayedKey) == nil){
            return 0
        }
        
        return UserDefaults.standard.integer(forKey: gamesPlayedKey)
    }
    
    static func getBadMarblesHit() -> Int{
        if (UserDefaults.standard.object(forKey: badMarblesHitKey) == nil){
            return 0
        }
        
        return UserDefaults.standard.integer(forKey: badMarblesHitKey)
    }
    
    static func getHighestTier() -> Int{
        if (UserDefaults.standard.object(forKey: highestTierKey) == nil){
            return 1
        }
        
        return UserDefaults.standard.integer(forKey: highestTierKey)
    }
    
    static func setBiggestStreak(streak: Int){
        if (streak > getBiggestStreak()){
            UserDefaults.standard.set(streak, forKey: biggestStreakKey)
        }
    }
    
    static func setGamesPlayed(){
        let gamesPlayed =  getGamesPlayed()
        UserDefaults.standard.set(gamesPlayed + 1, forKey: gamesPlayedKey)
    }
    
    static func setBadMarblesHit(badMarblesHit: Int){
        let currentBadMarblesHit = getBadMarblesHit()
        UserDefaults.standard.set(currentBadMarblesHit + badMarblesHit, forKey: badMarblesHitKey)
    }
    
    static func setHighestTier(tier: Int){
        if (tier > getHighestTier()){
            UserDefaults.standard.set(tier, forKey: highestTierKey)
        }
    }
    
    static func showInfoPopup(){
        let highScore = ScoreService.getHighScore()
        let totalPoints = ScoreService.getTotalPoints()
        let gamesPlayed = getGamesPlayed()
        let biggestStreak = getBiggestStreak()
        let badMarblesHit = getBadMarblesHit()
        let highestTier = getHighestTier()
        let pointsPerGame = gamesPlayed > 0 ? totalPoints/gamesPlayed : 0
        
        let statsMessage =
            "\nHigh Score: " + highScore.format() + "\n" +
                "Total Points: " + totalPoints.format() + "\n" +
                "Games Played: " + gamesPlayed.format() + "\n\n" +
                "Biggest Streak: " + biggestStreak.format() + "x\n" +
                "Highest Tier: " + highestTier.format() + "\n" +
                "Bad Marbles Hit: " + badMarblesHit.format() + "\n\n" +
                "Avg. Score Per Game: " + pointsPerGame.format()
        AlertService.showAlert(title: "Statistics", message: statsMessage)
    }
}
