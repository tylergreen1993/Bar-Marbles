//
//  IAPService.swift
//  Bad Marbles
//
//  Created by Tyler Green on 7/14/18.
//  Copyright © 2018 Tyler Green. All rights reserved.
//

import UIKit
import StoreKit

class IAPService: NSObject {
    static let shared = IAPService()
    
    fileprivate var productsRequest = SKProductsRequest()
    fileprivate var iapProducts = [SKProduct]()
    fileprivate var unlockablesRepository = UnlockablesRepository()
    fileprivate var productID = ""

    func canMakePurchases() -> Bool {
        return SKPaymentQueue.canMakePayments()
    }
    
    func purchaseProduct(productId: String){
        if (iapProducts.count == 0) {
            AlertService.showAlert(title: "Purchase Failed", message: "Could not purchase. Try again later.")
            return
        }
        
        if (canMakePurchases()) {
            if let product = iapProducts.filter({$0.productIdentifier == productId}).first{
                productID = productId
                let payment = SKPayment(product: product)
                SKPaymentQueue.default().add(self)
                SKPaymentQueue.default().add(payment)
            }
            else{
               AlertService.showAlert(title: "Purchase Failed", message: "Could not purchase. Try again later.")
            }
        }
        else{
            AlertService.showAlert(title: "Purchase Failed", message: "You must enable in-app purchases on your phone.")
        }
    }
    
    func restorePurchases(){
        SKPaymentQueue.default().add(self)
        SKPaymentQueue.default().restoreCompletedTransactions()
    }
    
    func fetchAvailableProducts(){
        let productIdentifiers = Set(unlockablesRepository.getUnlockablePurchaseIds())
            
        productsRequest = SKProductsRequest(productIdentifiers: productIdentifiers)
        productsRequest.delegate = self
        productsRequest.start()
    }
    
    func isProductPurchased(productId: String) -> Bool{
        return UserDefaults.standard.bool(forKey: productId)
    }
    
    func getProducts() -> [SKProduct]{
        return iapProducts
    }
    
    private func saveProduct(productId: String){
        if (!productId.isEmpty){
            UserDefaults.standard.set(true, forKey: productId)
        }
    }
}

extension IAPService: SKProductsRequestDelegate, SKPaymentTransactionObserver{
    func productsRequest (_ request:SKProductsRequest, didReceive response:SKProductsResponse) {
        if (response.products.count > 0) {
            iapProducts = response.products
        }
    }
    
    func paymentQueueRestoreCompletedTransactionsFinished(_ queue: SKPaymentQueue) {
        for trans in queue.transactions{
            saveProduct(productId: trans.payment.productIdentifier)
        }
        
        if (queue.transactions.count > 0){
            AlertService.showAlert(title: "Purchases Restored", message: "Your purchases were successfully restored.")
        }
        else{
            AlertService.showAlert(title: "No Purchases", message: "There are no purchases on your account to restore.")
        }
    }
    
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        for transaction:AnyObject in transactions {
            if let trans = transaction as? SKPaymentTransaction {
                switch trans.transactionState {
                case .purchased:
                    SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                    saveProduct(productId: productID)
                    Sounds.play(sound: Sounds.purchased, soundType: .sound)
                    NotificationCenter.default.post(name: Notification.Name("PurchaseSuccessful"), object: nil, userInfo: ["productID" : productID])
                    break
                case .failed:
                    SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                    AlertService.showAlert(title: "Purchase Failed", message: "Could not purchase. Try again later.")
                    break
                case .restored:
                    SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                    break
                default: break
                }
            }
        }
    }
}
