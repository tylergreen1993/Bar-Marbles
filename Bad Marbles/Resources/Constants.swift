//
//  Constants.swift
//  Bad Marbles
//
//  Created by Tyler Green on 4/21/18.
//  Copyright © 2018 Tyler Green. All rights reserved.
//

import UIKit

class Constants{
    static let labelPadding: CGFloat = 20
    static let boardPadding: CGFloat = 50
    
    static let marbleDistance: CGFloat = 1500
    static let marblesPerScreen = 5.0
    static let marbleTimeDecrease: Double = 0.9996
    
    static let scoreViewHeight: CGFloat = 90
    static let scoreViewStartHeight: CGFloat = 15

    static let lifeWidth: CGFloat = 25
    static let lifeHeight: CGFloat = 23
    
    static let waitToStart = 3.0
    static let newTierTime = 15.0
}
