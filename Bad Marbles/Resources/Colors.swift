//
//  Colors.swift
//  Bad Marbles
//
//  Created by Tyler Green on 4/21/18.
//  Copyright © 2018 Tyler Green. All rights reserved.
//

import UIKit

class Colors{
    static let scoreViewBlue = UIColor(displayP3Red: 52/255, green: 152/255, blue: 219/255, alpha: 1)
    static let logoRed = UIColor(displayP3Red: 188/255, green: 30/255, blue: 29/255, alpha: 0.85)
    static let softPurple = UIColor(displayP3Red: 84/255, green: 78/255, blue: 158/255, alpha: 0.85)
    static let darkGreen =  UIColor(displayP3Red: 78/255, green: 159/255, blue: 66/255, alpha: 1)
    static let badMarblesBlue =  UIColor(displayP3Red: 70/255, green: 142/255, blue: 188/255, alpha: 1)
    static let lighterBadMarblesBlue =  UIColor(displayP3Red: 90/255, green: 165/255, blue: 240/255, alpha: 1)
    static let unlockablesPink =  UIColor(displayP3Red: 154/255, green: 70/255, blue: 139/255, alpha: 1)
    static let darkPurple =  UIColor(displayP3Red: 44/255, green: 36/255, blue: 80/255, alpha: 1)
    static let scoreGreen =  UIColor(displayP3Red: 70/255, green: 228/255, blue: 60/255, alpha: 1)
}
