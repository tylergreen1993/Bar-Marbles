//
//  Globals.swift
//  Bad Marbles
//
//  Created by Tyler Green on 5/15/18.
//  Copyright © 2018 Tyler Green. All rights reserved.
//

import UIKit

class Globals{
    static var marbleTime: Double = 0
    static var marbleGenerateWaitTime = 0.0
    static var streak = 0
    
    static func resetGlobals(){
        marbleTime = defaultMarbleTime()
        marbleGenerateWaitTime = 0.0
        streak = 0
    }
    
    static func defaultMarbleTime() -> Double{
        return 8.5
    }
}
