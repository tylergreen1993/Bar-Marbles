//
//  Sounds.swift
//  Bad Marbles
//
//  Created by Tyler Green on 5/15/18.
//  Copyright © 2018 Tyler Green. All rights reserved.
//

import UIKit
import AudioToolbox

class Sounds{
    static let backgroundGameMusic = Sound(url: Bundle.main.url(forResource: "BackgroundMusic", withExtension: "mp3")!)
    static let menuMusic = Sound(url: Bundle.main.url(forResource: "MenuMusic", withExtension: "mp3")!)
    static let badMarblesTheme = Sound(url: Bundle.main.url(forResource: "BadMarblesTheme", withExtension: "mp3")!)
    static let badMarblesAcoustic = Sound(url: Bundle.main.url(forResource: "BadMarblesAcoustic", withExtension: "mp3")!)
    static let spokenWord = Sound(url: Bundle.main.url(forResource: "SpokenWord", withExtension: "mp3")!)
    static let marblesAnthem = Sound(url: Bundle.main.url(forResource: "MarblesAnthem", withExtension: "mp3")!)
    static let bohemianMarbles = Sound(url: Bundle.main.url(forResource: "BohemianMarbles", withExtension: "mp3")!)
    static let marblesTrophy = Sound(url: Bundle.main.url(forResource: "MarblesTrophy", withExtension: "mp3")!)
    static let secretTheme = Sound(url: Bundle.main.url(forResource: "MarblesSecretTheme", withExtension: "mp3")!)
    static let destroySound = Sound(url: Bundle.main.url(forResource: "DestroySound", withExtension: "mp3")!)
    static let lifeLost = Sound(url: Bundle.main.url(forResource: "LifeLost", withExtension: "mp3")!)
    static let explosion = Sound(url: Bundle.main.url(forResource: "Explosion", withExtension: "mp3")!)
    static let lifeGained = Sound(url: Bundle.main.url(forResource: "LifeGained", withExtension: "mp3")!)
    static let streak = Sound(url: Bundle.main.url(forResource: "Streak", withExtension: "mp3")!)
    static let gameStart = Sound(url: Bundle.main.url(forResource: "GameStart", withExtension: "mp3")!)
    static let newHighScore = Sound(url: Bundle.main.url(forResource: "NewHighScore", withExtension: "mp3")!)
    static let woosh = Sound(url: Bundle.main.url(forResource: "Woosh", withExtension: "mp3")!)
    static let select = Sound(url: Bundle.main.url(forResource: "Select", withExtension: "mp3")!)
    static let error = Sound(url: Bundle.main.url(forResource: "Error", withExtension: "mp3")!)
    static let purchased = Sound(url: Bundle.main.url(forResource: "Purchased", withExtension: "mp3")!)
    static let newTier = Sound(url: Bundle.main.url(forResource: "NewLevel", withExtension: "mp3")!)
    static let goldTrophy = Sound(url: Bundle.main.url(forResource: "GoldTrophy", withExtension: "mp3")!)
    
    static func play(sound: Sound?, soundType: SoundType){
        if (soundType == .sound && SoundService.getSoundEnabled()){
            DispatchQueue.global(qos: .background).async {
                sound?.play()
            }
        }
        else if (soundType == .music && SoundService.getMusicEnabled()){
            sound?.play(numberOfLoops: -1, completion: nil)
        }
    }
    
    static func addVibration(){
        AudioServicesPlaySystemSound(1520)
    }
    
    static func stopAllMusic(){
        backgroundGameMusic?.stop()
        badMarblesTheme?.stop()
        badMarblesAcoustic?.stop()
        marblesAnthem?.stop()
        bohemianMarbles?.stop()
        spokenWord?.stop()
        menuMusic?.stop()
        marblesTrophy?.stop()
        secretTheme?.stop()
    }
}

enum SoundType{
    case sound
    case music
}
