//
//  UnlockablesRepository.swift
//  Bad Marbles
//
//  Created by Tyler Green on 7/5/18.
//  Copyright © 2018 Tyler Green. All rights reserved.
//

import UIKit

class UnlockablesRepository{
    private var unlockables = [UnlockableSquare]()
    
    init(){
        createUnlockables()
    }
    
    func getUnlockables() -> [UnlockableSquare]{
        return unlockables
    }
    
    func getUnlockablesByMaxPoints(points: Int) -> [UnlockableSquare]{
        let qualifyingUnlockables = getUnlockables().filter({$0.points <= points || IAPService.shared.isProductPurchased(productId: $0.purchaseId)})
        
        return qualifyingUnlockables
    }
    
    func getNextAvailableUnlockable(points: Int) -> UnlockableSquare?{
        let unlockedUnlockables = getUnlockablesByMaxPoints(points: points)
        var lockedUnlockables = getUnlockables().filter({points >= $0.pointsToShow}).difference(from: unlockedUnlockables)
        lockedUnlockables.sort(by: {($1.points) > ($0.points)})
        if let firstNewUnlockable = lockedUnlockables.first{
            return firstNewUnlockable
        }
        return nil
    }
    
    func getUnlockablePurchaseIds() -> [String]{
        return unlockables.filter({!$0.purchaseId.isEmpty}).map({$0.purchaseId})
    }
    
    private func createUnlockables(){
        let noAdsSquare = UnlockableSquare()
        noAdsSquare.title = "No Ads"
        noAdsSquare.category = UnlockableType.misc
        noAdsSquare.points = 22500
        noAdsSquare.image = #imageLiteral(resourceName: "No Ads")
        noAdsSquare.id = 0
        noAdsSquare.purchaseId = "NoAds"
        unlockables.append(noAdsSquare)
        
        let extraLifeSquare = UnlockableSquare()
        extraLifeSquare.title = "Extra Life"
        extraLifeSquare.category = UnlockableType.misc
        extraLifeSquare.points = 3000
        extraLifeSquare.image = #imageLiteral(resourceName: "ExtraLife")
        extraLifeSquare.id = 1
        extraLifeSquare.purchaseId = "ExtraLife"
        unlockables.append(extraLifeSquare)
        
        let badMarblesDefaultSquare = UnlockableSquare()
        badMarblesDefaultSquare.title = "Default"
        badMarblesDefaultSquare.category = UnlockableType.music
        badMarblesDefaultSquare.points = 0
        badMarblesDefaultSquare.image = #imageLiteral(resourceName: "Musical Marble 3")
        badMarblesDefaultSquare.id = 2
        unlockables.append(badMarblesDefaultSquare)
        
        let badMarblesThemeSquare = UnlockableSquare()
        badMarblesThemeSquare.title = "Rockin' Marbles"
        badMarblesThemeSquare.category = UnlockableType.music
        badMarblesThemeSquare.points = 125
        badMarblesThemeSquare.image = #imageLiteral(resourceName: "Musical Marble")
        badMarblesThemeSquare.id = 3
        badMarblesThemeSquare.purchaseId = "BadMarblesTheme"
        unlockables.append(badMarblesThemeSquare)
        
        let badMarblesAcousticSquare = UnlockableSquare()
        badMarblesAcousticSquare.title = "Acoustic Rambles"
        badMarblesAcousticSquare.category = UnlockableType.music
        badMarblesAcousticSquare.points = 1500
        badMarblesAcousticSquare.image = #imageLiteral(resourceName: "Musical Marble 2")
        badMarblesAcousticSquare.id = 4
        badMarblesAcousticSquare.purchaseId = "AcousticRambles"
        unlockables.append(badMarblesAcousticSquare)
        
        let defaultMarblesSquare = UnlockableSquare()
        defaultMarblesSquare.title = "Default"
        defaultMarblesSquare.category = UnlockableType.marbles
        defaultMarblesSquare.points = 0
        defaultMarblesSquare.image = #imageLiteral(resourceName: "Green Marble")
        defaultMarblesSquare.id = 5
        unlockables.append(defaultMarblesSquare)
        
        let foodMarblesSquare = UnlockableSquare()
        foodMarblesSquare.title = "Food"
        foodMarblesSquare.category = UnlockableType.marbles
        foodMarblesSquare.points = 6000
        foodMarblesSquare.image = #imageLiteral(resourceName: "Green Food")
        foodMarblesSquare.id = 6
        foodMarblesSquare.purchaseId = "FoodMarbles"
        unlockables.append(foodMarblesSquare)
        
        let defaultBackgroundSquare = UnlockableSquare()
        defaultBackgroundSquare.title = "Default"
        defaultBackgroundSquare.category = UnlockableType.backgrounds
        defaultBackgroundSquare.points = 0
        defaultBackgroundSquare.image = #imageLiteral(resourceName: "View Background")
        defaultBackgroundSquare.id = 7
        unlockables.append(defaultBackgroundSquare)
        
        let orangeMarbleSquare = UnlockableSquare()
        orangeMarbleSquare.title = "2x Points Marble"
        orangeMarbleSquare.category = UnlockableType.misc
        orangeMarbleSquare.points = 500
        orangeMarbleSquare.image = #imageLiteral(resourceName: "Orange Marble")
        orangeMarbleSquare.id = 8
        orangeMarbleSquare.purchaseId = "2xPoints"
        unlockables.append(orangeMarbleSquare)
        
        let kaleidoscopeBackgroundSquare = UnlockableSquare()
        kaleidoscopeBackgroundSquare.title = "Kaleidoscope"
        kaleidoscopeBackgroundSquare.category = UnlockableType.backgrounds
        kaleidoscopeBackgroundSquare.points = 1000
        kaleidoscopeBackgroundSquare.image = #imageLiteral(resourceName: "Kaleidoscope Background")
        kaleidoscopeBackgroundSquare.id = 9
        kaleidoscopeBackgroundSquare.purchaseId = "Kaleidoscope"
        unlockables.append(kaleidoscopeBackgroundSquare)
        
        let championBackgroundSquare = UnlockableSquare()
        championBackgroundSquare.title = "Champion"
        championBackgroundSquare.category = UnlockableType.backgrounds
        championBackgroundSquare.points = 11000
        championBackgroundSquare.image = #imageLiteral(resourceName: "Champion Background")
        championBackgroundSquare.id = 10
        championBackgroundSquare.purchaseId = "Champion"
        unlockables.append(championBackgroundSquare)
        
        let creatorsMessageSquare = UnlockableSquare()
        creatorsMessageSquare.title = "Creator's Message"
        creatorsMessageSquare.category = UnlockableType.music
        creatorsMessageSquare.points = 20000
        creatorsMessageSquare.image = #imageLiteral(resourceName: "Music Marble 4")
        creatorsMessageSquare.id = 11
        creatorsMessageSquare.purchaseId = "CreatorsMessage"
        unlockables.append(creatorsMessageSquare)
        
        let glowingMarblesSquare = UnlockableSquare()
        glowingMarblesSquare.title = "Glowing Marbles"
        glowingMarblesSquare.category = UnlockableType.misc
        glowingMarblesSquare.points = 0
        glowingMarblesSquare.image = #imageLiteral(resourceName: "Glowing Marbles")
        glowingMarblesSquare.id = 12
        unlockables.append(glowingMarblesSquare)
        
        let emojiMarblesSquare = UnlockableSquare()
        emojiMarblesSquare.title = "Emoji"
        emojiMarblesSquare.category = UnlockableType.marbles
        emojiMarblesSquare.points = 2000
        emojiMarblesSquare.image = #imageLiteral(resourceName: "Green Emoji")
        emojiMarblesSquare.id = 13
        emojiMarblesSquare.purchaseId = "Emoji"
        unlockables.append(emojiMarblesSquare)
        
        let marblesAnthemSquare = UnlockableSquare()
        marblesAnthemSquare.title = "Marbles Anthem"
        marblesAnthemSquare.category = UnlockableType.music
        marblesAnthemSquare.points = 4000
        marblesAnthemSquare.image = #imageLiteral(resourceName: "Musical Marble 5")
        marblesAnthemSquare.id = 14
        marblesAnthemSquare.purchaseId = "MarblesAnthem"
        unlockables.append(marblesAnthemSquare)
        
        let marblesTrophySquare = UnlockableSquare()
        marblesTrophySquare.title = "Marbles Trophy"
        marblesTrophySquare.category = UnlockableType.misc
        marblesTrophySquare.points = 25000
        marblesTrophySquare.image = #imageLiteral(resourceName: "Marbles Trophy")
        marblesTrophySquare.id = 15
        marblesTrophySquare.purchaseId = "MarblesTrophy"
        unlockables.append(marblesTrophySquare)
        
        let spaceBackgroundSquare = UnlockableSquare()
        spaceBackgroundSquare.title = "Nebula"
        spaceBackgroundSquare.category = UnlockableType.backgrounds
        spaceBackgroundSquare.points = 8500
        spaceBackgroundSquare.image = #imageLiteral(resourceName: "Space Background")
        spaceBackgroundSquare.id = 16
        spaceBackgroundSquare.purchaseId = "Nebula"
        unlockables.append(spaceBackgroundSquare)
        
        let sportsMarblesSquare = UnlockableSquare()
        sportsMarblesSquare.title = "Sports"
        sportsMarblesSquare.category = UnlockableType.marbles
        sportsMarblesSquare.points = 3500
        sportsMarblesSquare.image = #imageLiteral(resourceName: "Green Sports")
        sportsMarblesSquare.id = 17
        sportsMarblesSquare.purchaseId = "SportsMarble"
        unlockables.append(sportsMarblesSquare)
        
        let badMarblesBackgroundSquare = UnlockableSquare()
        badMarblesBackgroundSquare.title = "Many Marbles"
        badMarblesBackgroundSquare.category = UnlockableType.backgrounds
        badMarblesBackgroundSquare.points = 25
        badMarblesBackgroundSquare.image = #imageLiteral(resourceName: "Bad Marbles Background")
        badMarblesBackgroundSquare.id = 18
        badMarblesBackgroundSquare.purchaseId = "BadMarblesBG"
        unlockables.append(badMarblesBackgroundSquare)
        
        let outlineMarblesSquare = UnlockableSquare()
        outlineMarblesSquare .title = "Outline"
        outlineMarblesSquare .category = UnlockableType.marbles
        outlineMarblesSquare .points = 12500
        outlineMarblesSquare .image = #imageLiteral(resourceName: "Green Outline")
        outlineMarblesSquare .id = 19
        outlineMarblesSquare.purchaseId = "OutlineMarble"
        unlockables.append(outlineMarblesSquare)
        
        let noBombsSquare = UnlockableSquare()
        noBombsSquare.title = "No Bombs"
        noBombsSquare.category = UnlockableType.misc
        noBombsSquare.points = 10000
        noBombsSquare.image = #imageLiteral(resourceName: "No Bombs")
        noBombsSquare.id = 20
        noBombsSquare.purchaseId = "NoBombs"
        unlockables.append(noBombsSquare)
        
        let bohemianMarblesSquare = UnlockableSquare()
        bohemianMarblesSquare.title = "Bohemian Marbles"
        bohemianMarblesSquare.category = UnlockableType.music
        bohemianMarblesSquare.points = 7000
        bohemianMarblesSquare.image = #imageLiteral(resourceName: "Musical Marble 6")
        bohemianMarblesSquare.id = 21
        bohemianMarblesSquare.purchaseId = "BohemianMarbles"
        unlockables.append(bohemianMarblesSquare)
        
        let casinoMarblesSquare = UnlockableSquare()
        casinoMarblesSquare.title = "Casino"
        casinoMarblesSquare.category = UnlockableType.marbles
        casinoMarblesSquare.points = 750
        casinoMarblesSquare.image = #imageLiteral(resourceName: "Green Casino")
        casinoMarblesSquare.id = 22
        casinoMarblesSquare.purchaseId = "CasinoMarbles"
        unlockables.append(casinoMarblesSquare)
        
        let llamaBackgroundSquare = UnlockableSquare()
        llamaBackgroundSquare.title = "Llama"
        llamaBackgroundSquare.category = UnlockableType.backgrounds
        llamaBackgroundSquare.points = 2500
        llamaBackgroundSquare.image = #imageLiteral(resourceName: "Llama Background")
        llamaBackgroundSquare.id = 23
        llamaBackgroundSquare.purchaseId = "LlamaBackground"
        unlockables.append(llamaBackgroundSquare)
        
        let extraLife2Square = UnlockableSquare()
        extraLife2Square.title = "Extra Life #2"
        extraLife2Square.category = UnlockableType.misc
        extraLife2Square.points = 15000
        extraLife2Square.image = #imageLiteral(resourceName: "ExtraLife2")
        extraLife2Square.id = 24
        extraLife2Square.purchaseId = "ExtraLife2"
        unlockables.append(extraLife2Square)
        
        let pointedTiersSquare = UnlockableSquare()
        pointedTiersSquare.title = "Pointed Tiers"
        pointedTiersSquare.category = UnlockableType.misc
        pointedTiersSquare.points = 17000
        pointedTiersSquare.image = #imageLiteral(resourceName: "Pointed Tier")
        pointedTiersSquare.id = 25
        pointedTiersSquare.purchaseId = "PointedTiers"
        unlockables.append(pointedTiersSquare)
        
        let doubleExtraChance = UnlockableSquare()
        doubleExtraChance.title = "2x Extra Chance"
        doubleExtraChance.category = UnlockableType.misc
        doubleExtraChance.points = 5000
        doubleExtraChance.image = #imageLiteral(resourceName: "2xExtraChance")
        doubleExtraChance.id = 26
        doubleExtraChance.purchaseId = "2xExtraChance"
        unlockables.append(doubleExtraChance)
        
        let secretSongSquare = UnlockableSquare()
        secretSongSquare.title = "Secret Theme"
        secretSongSquare.category = UnlockableType.music
        secretSongSquare.points = 27500
        secretSongSquare.image = #imageLiteral(resourceName: "Musical Marble 7")
        secretSongSquare.id = 27
        secretSongSquare.pointsToShow = 25000
        secretSongSquare.purchaseId = "SecretTheme"
        unlockables.append(secretSongSquare)
        
        let secretLogoSquare = UnlockableSquare()
        secretLogoSquare.title = "Secret Logo"
        secretLogoSquare.category = UnlockableType.misc
        secretLogoSquare.points = 30000
        secretLogoSquare.image = #imageLiteral(resourceName: "Secret Logo")
        secretLogoSquare.id = 28
        secretLogoSquare.pointsToShow = 25000
        secretLogoSquare.purchaseId = "SecretLogo"
        unlockables.append(secretLogoSquare)
        
        let secretMarbleSquare = UnlockableSquare()
        secretMarbleSquare.title = "Secret Marble"
        secretMarbleSquare.category = UnlockableType.marbles
        secretMarbleSquare.points = 50000
        secretMarbleSquare.image = #imageLiteral(resourceName: "Green Secret")
        secretMarbleSquare.id = 29
        secretMarbleSquare.pointsToShow = 45000
        secretMarbleSquare.purchaseId = "SecretMarble"
        unlockables.append(secretMarbleSquare)
        
        let secretScribblesSquare = UnlockableSquare()
        secretScribblesSquare.title = "Secret Scribbles"
        secretScribblesSquare.category = UnlockableType.backgrounds
        secretScribblesSquare.points = 40000
        secretScribblesSquare.image = #imageLiteral(resourceName: "Scribbles Background")
        secretScribblesSquare.id = 30
        secretScribblesSquare.pointsToShow = 35000
        secretScribblesSquare.purchaseId = "SecretScribbles"
        unlockables.append(secretScribblesSquare)
    }
}
