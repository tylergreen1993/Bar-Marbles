//
//  AchievementsRepository.swift
//  Bad Marbles
//
//  Created by Tyler Green on 10/26/18.
//  Copyright © 2018 Tyler Green. All rights reserved.
//

import UIKit

class AchievementsRepository{
    private var achievements = [AchievementDetails]()
    
    init(){
        createAchievements()
    }
    
    func getAchievements() -> [AchievementDetails]{
        return achievements
    }
    
    func reloadAchievements(){
        createAchievements()
    }

    private func createAchievements(){
        achievements = [AchievementDetails]()
        
        let highScore = Float(ScoreService.getHighScore())
        let totalPoints = Float(ScoreService.getTotalPoints())
        let gamesPlayed = Float(StatisticsService.getGamesPlayed())
        let biggestStreak = Float(StatisticsService.getBiggestStreak())
        let highestTier = Float(StatisticsService.getHighestTier())
        
        achievements.append(AchievementDetails(name: "250 High Score", difficulty: .bronze, category: .highScore, ratio: highScore/250))
        achievements.append(AchievementDetails(name: "500 High Score", difficulty: .bronze2, category: .highScore, ratio: highScore/500))
        achievements.append(AchievementDetails(name: "1,000 High Score", difficulty: .silver, category: .highScore, ratio: highScore/1000))
        achievements.append(AchievementDetails(name: "2,500 High Score", difficulty: .silver2, category: .highScore, ratio: highScore/2500))
        achievements.append(AchievementDetails(name: "5,000 High Score", difficulty: .gold, category: .highScore, ratio: highScore/5000))
        
        achievements.append(AchievementDetails(name: "1,000 Points", difficulty: .bronze, category: .points, ratio: totalPoints/1000))
        achievements.append(AchievementDetails(name: "5,000 Points", difficulty: .bronze2,  category: .points, ratio: totalPoints/5000))
        achievements.append(AchievementDetails(name: "20,000 Points", difficulty: .silver,  category: .points, ratio: totalPoints/20000))
        achievements.append(AchievementDetails(name: "50,000 Points", difficulty: .silver2,  category: .points, ratio: totalPoints/50000))
        achievements.append(AchievementDetails(name: "100,000 Points", difficulty: .gold,  category: .points, ratio: totalPoints/100000))
        
        achievements.append(AchievementDetails(name: "10 Games Played", difficulty: .bronze, category: .games, ratio: gamesPlayed/10))
        achievements.append(AchievementDetails(name: "25 Games Played", difficulty: .bronze2, category: .games, ratio: gamesPlayed/25))
        achievements.append(AchievementDetails(name: "50 Games Played", difficulty: .silver, category: .games, ratio: gamesPlayed/50))
        achievements.append(AchievementDetails(name: "100 Games Played", difficulty: .silver2, category: .games, ratio: gamesPlayed/100))
        achievements.append(AchievementDetails(name: "200 Games Played", difficulty: .gold, category: .games, ratio: gamesPlayed/200))
        
        achievements.append(AchievementDetails(name: "5x Streak", difficulty: .bronze, category: .streak, ratio: biggestStreak/5))
        achievements.append(AchievementDetails(name: "10x Streak", difficulty: .bronze2, category: .streak, ratio: biggestStreak/10))
        achievements.append(AchievementDetails(name: "20x Streak", difficulty: .silver, category: .streak, ratio: biggestStreak/20))
        achievements.append(AchievementDetails(name: "25x Streak", difficulty: .silver2, category: .streak, ratio: biggestStreak/25))
        achievements.append(AchievementDetails(name: "30x Streak", difficulty: .gold, category: .streak, ratio: biggestStreak/30))
        
        achievements.append(AchievementDetails(name: "3rd Tier Hit", difficulty: .bronze, category: .tier, ratio: highestTier/3))
        achievements.append(AchievementDetails(name: "5th Tier Hit", difficulty: .bronze2, category: .tier, ratio: highestTier/5))
        achievements.append(AchievementDetails(name: "8th Tier Hit", difficulty: .silver, category: .tier, ratio: highestTier/8))
        achievements.append(AchievementDetails(name: "10th Tier Hit", difficulty: .silver2, category: .tier, ratio: highestTier/10))
        achievements.append(AchievementDetails(name: "15th Tier Hit", difficulty: .gold, category: .tier, ratio: highestTier/15))
    }
}

class AchievementDetails{
    var name: String
    var difficulty: AchievementDifficulty
    var category: AchievementCategory
    var ratio: Float
    
    init(name: String, difficulty: AchievementDifficulty, category: AchievementCategory, ratio: Float){
        self.name = name
        self.difficulty = difficulty
        self.category = category
        self.ratio = ratio
    }
}

enum AchievementDifficulty: Int{
    case bronze, bronze2, silver, silver2, gold
}

enum AchievementCategory: Int{
    case highScore, points, games, streak, tier
}
